/*
 * sensors.h
 *
 *  Created on: 4Sep.,2018
 *      Author: Jezz
 */

#ifndef PROJECTS_PINGPONG_DATA_STRUCTS_H_
#define PROJECTS_PINGPONG_DATA_STRUCTS_H_

#include "hal_types.h"
#include <string.h>
#include "hw.h"
#include "radio.h"
#include "timeServer.h"
#include "delay.h"
#include "low_power_manager.h"
#include "vcom.h"
#include "stm32l0xx_hal_rtc.h"


typedef struct
{

uint32_t currentTime;

uint32_t reportInterval;

uint32_t sleepInterval;

uint16_t totalNodes;

uint32_t userAck;

uint8_t currentUser;

uint32_t updateReport;

int bleForcedSample;

uint32_t sleepTime;

} GatewayData_t;

typedef struct
{
	double gradient;
	double intercept;
} TankInterpolation_t;

typedef struct
{

uint16_t* tankReadings;

double* temperatureReadings;

uint16_t* averageCurrentReadings;

uint16_t* battVoltageReadings;

TankInterpolation_t* tankInters;

uint16_t* tankThresholds;

uint8_t* tankHasError;

} SensorData_t;

typedef enum
{
GATEWAY_TRIG_SUCCESS,
GATEWAY_TRIG_FAILURE,
GATEWAY_DATA_RECV,
GATEWAY_DATA_WAITING,
GATEWAY_DATA_TIMEOUT,
GATEWAY_DATA_ERROR,
GATEWAY_DATA_SEND,
GATEWAY_DATA_TX,
SENSORS_OKAY,
SENSORS_FAIL,
SENSORS_ERROR,
MCU_NORMAL_SLEEPING,
MCU_ERROR_SLEEPING,
BLE_WAKEUP

}currentStatus_t;

typedef enum
{
HELP,
TANKS,
TEMPS,
LEVEL,
SET_NUM,
NUMS,
REMOVE,
NODES,
REPORT

} messageType_t;


#endif /* PROJECTS_PINGPONG_DATA_STRUCTS_H_ */
