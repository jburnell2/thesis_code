/*
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
    (C)2013 Semtech

Description: Ping-Pong implementation

License: Revised BSD License, see LICENSE.TXT file include in the project

Maintainer: Miguel Luis and Gregory Cristian
*/
/******************************************************************************
  * @file    main.c
  * @author  MCD Application Team
  * @version V1.1.4
  * @date    08-January-2018
  * @brief   this is the main!
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Packet Structure
 *
 * #|dest address (node)|source address|payload|#
 *
 *
 */

/* Includes ------------------------------------------------------------------*/
#include "stm32l072_i2c.h"
#include <string.h>
#include "hw.h"
#include "radio.h"
#include "timeServer.h"
#include "delay.h"
#include "low_power_manager.h"
#include "vcom.h"
#include "stm32l0xx_hal_rtc.h"
#include "bq27441_defs.h"
#include "stm32l0xx_hal_iwdg.h"
#include "stm32l0xx_hal_i2c_ex.h"


#define RF_FREQUENCY                                915000000 // Hz

#define TX_OUTPUT_POWER                             14        // dBm


#define LORA_BANDWIDTH                              0         // [0: 125 kHz,
                                                              //  1: 250 kHz,
                                                              //  2: 500 kHz,
                                                              //  3: Reserved]
#define LORA_SPREADING_FACTOR                       7         // [SF7..SF12]
#define LORA_CODINGRATE                             1         // [1: 4/5,
                                                              //  2: 4/6,
                                                        	  //  3: 4/7,
                                                              //  4: 4/8]
#define LORA_PREAMBLE_LENGTH                        8         // Same for Tx and Rx
#define LORA_SYMBOL_TIMEOUT                         5         // Symbols
#define LORA_FIX_LENGTH_PAYLOAD_ON                  false
#define LORA_IQ_INVERSION_ON                        false

#define RX_TIMEOUT_VALUE          	10*CLOCK_SECOND
#define BUFFER_SIZE                	64 // Define the payload size here
#define CLOCK_SECOND               1000
#define CLOCK_MINUTE              	60*CLOCK_SECOND

#define uS_10 490

#define NODE_ID "0x01"

#define BQ27441_ADDRESS 0x55 << 1 // TI Fuel Gauge
#define MCP9808_ADDRESS 0x18 << 1 // Temperature Sensor
#define DS2782_ADDRESS 0x34 << 1 // Maxim Fuel Gauge


typedef struct
{

int16_t sleepError;

int16_t initTimeDiff;

uint32_t reportInterval;

}NodeData_t;

typedef struct
{

uint16_t tankLevelMeasure;

uint16_t batteryVoltage;

int32_t avCurrent;

float temperature;

}SensorData_t;

typedef enum
{
NODE_INIT_SUCCESS,
NODE_INIT_FAILURE,
NODE_INIT_BAD,
NODE_DATA_RECV,
NODE_DATA_RECV_ERR,
NODE_DATA_WAITING,
NODE_DATA_TIMEOUT,
NODE_DATA_SENDING,
NODE_DATA_COMPLETE,
SENSORS_OKAY,
SENSORS_FAIL,

}returnStatus_t;

volatile returnStatus_t State;

uint32_t GPIOA_MODER, GPIOB_MODER, GPIOC_MODER;

RTC_HandleTypeDef hrtc;

uint16_t BufferSize = BUFFER_SIZE;
uint8_t Buffer[BUFFER_SIZE];

volatile int8_t RssiValue = 0;
volatile int8_t SnrValue = 0;

static I2C_HandleTypeDef hi2c1;
static uint32_t wakeTime = 0;

IWDG_HandleTypeDef hiwdg;

 /* Led Timers objects*/
//static  TimerEvent_t timerLed;

/* Private function prototypes -----------------------------------------------*/

/*!
 * Radio events function pointer
 */
static RadioEvents_t RadioEvents;

void OnTxDone( void );
void OnRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );
void OnTxTimeout( void );
void OnRxTimeout( void );
void OnRxError( void );
void Enter_LPRun( void );
void Enter_Stop( void );
void Sensors_Init( SensorData_t * sensor_data  );
uint16_t Read_Distance( void );
returnStatus_t Receive_Node_Data( NodeData_t * node_data, uint8_t init);
void LoraRadio_Init( void );
void Send_Data( NodeData_t * node_data, SensorData_t * sensor_data );
void Generate_Data( SensorData_t * sensor_data );
void wake_sensors( void );
static void MX_IWDG_Init(void);


/**
 * Main application entry point.
 */
int main( void )
{

  HAL_Init( );
  
  Enter_LPRun();
  
  DBG_Init( );

  HW_Init( );

  LoraRadio_Init();

  SensorData_t sensor_data;

  Sensors_Init(&sensor_data);

  NodeData_t node_data;

  node_data.reportInterval = CLOCK_SECOND*30;
  node_data.sleepError = 0;

  uint8_t firstRun = 1;
  uint32_t timeOutSleep = 0;

  uint32_t wakeTime = 0, sleepTime = 0;

  int ret;

  while (1)
  {
	  timeOutSleep = 0;
	  if (firstRun) {
		  // recv time
		  while (Receive_Node_Data(&node_data, 1) != NODE_INIT_SUCCESS);
		  wakeTime = HW_RTC_GetTimerSeconds();
	  }
	  //DelayMs(500);
	  Generate_Data(&sensor_data);
	  Send_Data(&node_data, &sensor_data);
	  //ret = 0;
	  ret = Receive_Node_Data(&node_data, firstRun);
	  if (ret == NODE_INIT_FAILURE || ret == NODE_INIT_BAD) {
		  // timed out waiting for gateway to send recv time, just sleep less time now
		  timeOutSleep = RX_TIMEOUT_VALUE + CLOCK_SECOND*35;
	  	  // Reset node to receive data to make sure sync is correct
		  firstRun = 1;

	  } else {
		  firstRun = 0;
  	  }

	  // Sleep and Low power mode
	  Radio.Sleep( );
	  PRINTF("SleepError = %d\n\r", node_data.sleepError);
	  sleepTime = HW_RTC_GetTimerSeconds();

	  int32_t sleepForMs = node_data.reportInterval +
		  	  (node_data.sleepError)*CLOCK_SECOND - timeOutSleep;
			 // (sleepTime - wakeTime)*CLOCK_SECOND - timeOutSleep;

	  PRINTF("Prelim Sleeping for: %d\n\r", sleepForMs);
	  if ((sleepForMs <= 0) || (sleepForMs > (node_data.reportInterval + 15*CLOCK_SECOND))) {
		  sleepForMs = 1*CLOCK_MINUTE;
		  firstRun = 1;
	  }
	  PRINTF("Sleeping for: %d\n\r", sleepForMs);
	  HW_RTC_SetAlarm(HW_RTC_ms2Tick(sleepForMs));
	  Enter_Stop();
	  Enter_LPRun();
	  wakeTime = HW_RTC_GetTimerSeconds();
	  HW_RTC_StopAlarm();
	  wake_sensors();

  }
}

void wake_sensors( void ) {

	  uint8_t data[3];
	  data[0] = 0x01;
	  data[1] = 0x00;
	  data[2] = 0x18;
	  write_device(&hi2c1, MCP9808_ADDRESS, data, 3);

	  DelayMs(250);
}


uint16_t Read_Distance ( void )
{

	HAL_GPIO_DeInit(GPIOB, GPIO_PIN_14);

	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Pin = GPIO_PIN_14;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStructure.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);


	__HAL_RCC_TIM6_CLK_ENABLE();
	/* Clear the update event flag */
	TIM6->SR = 0;
	/* Set the required delay */
	/* The timer presclaer reset value is 0. If a longer delay is required the
	presacler register may be configured to */
	/*TIM6->PSC = 0 */
	TIM6->ARR = uS_10;

	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, 0);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, 1);

	/* Start the timer counter */
	TIM6->CR1 |= TIM_CR1_CEN;
	while (!(TIM6->SR & TIM_SR_UIF));
	TIM6->SR = 0;

	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, 0);

	HAL_GPIO_DeInit(GPIOB, GPIO_PIN_14);

	GPIO_InitStructure.Mode = GPIO_MODE_INPUT;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);

	uint32_t timeout = 8000, timing = 0;
	while (HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_14) == GPIO_PIN_RESET) {
		timing++;
		if (timing >= timeout) {
			return 0;
		}
	}

	TIM6->ARR = 0xFFFF;
	TIM6->CR1 &= ~TIM_CR1_CEN;
	TIM6->PSC = 32;
	/* Start the timer counter */
	TIM6->CR1 |= TIM_CR1_CEN;
	timing = 0;
	while (HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_14) == GPIO_PIN_SET) {
		timing++;
		if (timing >- timeout) {
			return 0;
		}
	}
	TIM6->CR1 &= ~TIM_CR1_CEN;
	uint32_t distance = TIM6->CNT;
	//PRINTF("Distance: %d cm \n\r", dist*18/1024);

	return distance*18/1024; // Divide by 58 for distance in cm

	}

void Sensors_Init( SensorData_t * sensor_data  )
{
	GPIO_InitTypeDef GPIO_InitStructure;

	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();

	// Ultrasonic Sensor

	// Trigger pin
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Pin = GPIO_PIN_14;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStructure.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);

	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, 0);

	GPIO_InitTypeDef GPIO_InitStructure2;

	// Echo pin
	GPIO_InitStructure2.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure2.Pin = GPIO_PIN_13;
	GPIO_InitStructure2.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStructure2.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStructure2);


	__I2C1_CLK_ENABLE();

	GPIO_InitStructure.Pin        = GPIO_PIN_8;
	GPIO_InitStructure.Mode       = GPIO_MODE_AF_OD;
	GPIO_InitStructure.Speed      = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStructure.Pull       = GPIO_PULLUP;
	GPIO_InitStructure.Alternate  = GPIO_AF4_I2C1;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);

	GPIO_InitStructure.Pin        = GPIO_PIN_9;
	GPIO_InitStructure.Mode       = GPIO_MODE_AF_OD;
	GPIO_InitStructure.Speed      = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStructure.Pull       = GPIO_PULLUP;
	GPIO_InitStructure.Alternate  = GPIO_AF4_I2C1;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);


	hi2c1.Instance = I2C1;
	hi2c1.Init.Timing =  0x00403D5A;
	hi2c1.Init.OwnAddress1 = 0;
	hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	hi2c1.Init.OwnAddress2 = 0;
	hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
	hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
	if (HAL_I2C_Init(&hi2c1) != HAL_OK)
	{
		  PRINTF("ERROR\n\r");
	}

	// Set up fuel guage parameters
	uint8_t data[3];
	data[0] = 69; // eeprom block 1 - RSNSP
	data[1] = 50; // 1/0.02 mhos (conductance)
	write_device(&hi2c1, DS2782_ADDRESS, data, 2);

	sensor_data->temperature = 0;
	sensor_data->tankLevelMeasure = 0;
	sensor_data->batteryVoltage = 0;
	sensor_data->avCurrent = 0;

}

/* IWDG init function */
static void MX_IWDG_Init(void)
{

  hiwdg.Instance = IWDG;
  hiwdg.Init.Prescaler = IWDG_PRESCALER_32;
  hiwdg.Init.Window = 4095;
  hiwdg.Init.Reload = 4095;
  //if (HAL_IWDG_Init(&hiwdg) != HAL_OK)
  //{

  //}

}

char** parse_packet(char* packet, uint8_t *length) {

	for (int i = 0; i < strlen(packet); i++) {
		// Store the number of packet contents
		if (packet[i] == '|') {
			(*length)++;
		}
	}
	// Store each packet
	char **packetContents = malloc((*length) * sizeof(char*));

	int j = 0, subIndex = 0;
	for (int l = 0; l < (*length); l++) {
		subIndex = 0;
		char subPacket[30];
		while (packet[j] != '|') {
			// go through each packet in the message
			subPacket[subIndex] = packet[j];
			subIndex++;
			j++;
		}
		subPacket[subIndex] = '\0';

		// store the packet in a string array
		packetContents[l] = malloc((subIndex+1) * sizeof(char));
		memcpy(packetContents[l], &subPacket[0], subIndex+1);
		j++;
	}

	return packetContents;

}

/*!
 * \brief Function that will receive the configuration properties
 * from the base station.
*/
returnStatus_t Receive_Node_Data( NodeData_t * node_data, uint8_t init) {

	State = NODE_DATA_WAITING;
	Radio.Rx( RX_TIMEOUT_VALUE );

	PRINTF("Waiting for init/ return packet...\n\r");

	while (State == NODE_DATA_WAITING);

	switch (State) {
		case NODE_DATA_RECV:
			break;
		case NODE_DATA_TIMEOUT:
			return NODE_INIT_FAILURE;
		case NODE_DATA_RECV_ERR:
			return NODE_INIT_SUCCESS;
		default:
			break;
	}

	if (BufferSize > 0) {
		char dest[5];
		char src[5];
		char payload[20];
		memcpy( src, &Buffer[5], 4 );
		memcpy( dest, &Buffer[0], 4 );
		src[4] = '\0';
		dest[4] = '\0';
		// Check if the packet is from gateway and for this node
		if (( strcmp(src, "0x00") == 0 ) && (strcmp(dest, NODE_ID) == 0)) {
			memcpy( payload, &Buffer[10], 19 );
			payload[19] = '\0';
			uint8_t numPackets = 0;
			char **packetContents = parse_packet(payload, &numPackets);
			if (packetContents[0][0] != 'I') { // Init packet
				node_data->reportInterval = atoi(packetContents[0]);

				node_data->sleepError = (int16_t)atoi(packetContents[1]);

				PRINTF("sleepError = %d\n\r", node_data->sleepError);

				// Now make sure node is synced with gateway
				if (init) {
					//int timerVal = HW_RTC_GetTimerSeconds();
					//node_data->initTimeDiff = (timerVal - atoi(packetContents[1]));
					//PRINTF("init:%d\n\r", node_data->initTimeDiff);
				} else {
					//int timerVal = HW_RTC_GetTimerSeconds();

					//node_data->sleepError = node_data->initTimeDiff -
					//		(timerVal - (uint32_t)atoi(packetContents[1]));
					//PRINTF("init:%d, timerVal:%d, packet:%d\n\r", node_data->initTimeDiff,
						//	timerVal, (uint32_t)atoi(packetContents[1]));
				}
			} else {
				if (!init) {
					PRINTF("BAD\n\r");
					return NODE_INIT_BAD;
				}
			}
		} else {
			return NODE_INIT_FAILURE;
		}
	} else {
		return NODE_INIT_FAILURE;
	}
	return NODE_INIT_SUCCESS;

}

/*!
 * \brief Function that will get all sensor data.
*/
void Generate_Data( SensorData_t * sensor_data ) {

	PRINTF("Generating Packet...\n\r");
	uint8_t i;
	uint8_t addr;

	//Temperature - MSB is [1] and LSB is [0]
	uint8_t temperature[2];
	addr = 0x05;
	float raw_temp;

	read_device(&hi2c1, MCP9808_ADDRESS, &addr, 3, temperature);

	temperature[0] = temperature[0] & 0x1F; //Clear flag bits
	if ((temperature[0] & 0x10) == 0x10){ //TA < 0°C
		temperature[0] = temperature[0] & 0x0F; //Clear SIGN
		raw_temp = 256 - (temperature[0] * 16 + ((float)temperature[1] / 16));
	} else {//TA ³ 0°C
		raw_temp = (temperature[0] * 16 + ((float)temperature[1] / 16));
	}
	addr = 0x01;
	sensor_data->temperature = raw_temp;
	PRINTF("Temperature: %f *C\n\r", raw_temp);

	DelayMs(100);

	// Fuel Gauge
	addr = 0x0C;
	uint8_t voltage[2];
	read_device(&hi2c1, DS2782_ADDRESS, &addr, 2, voltage);
	uint16_t raw_mV = (voltage[0] << 8) | (voltage[1]);
	raw_mV  = raw_mV >> 5;
	raw_mV = raw_mV*2499/512;
	PRINTF("Battery: %d mV\n\r", raw_mV);
	if (raw_mV > 9999) {
		sensor_data->batteryVoltage = 0000;
	} else {
		sensor_data->batteryVoltage = raw_mV;
	}
	DelayMs(1);

	addr = 0x08; // av
	//addr = 0x0E; // inst
	uint8_t rawCurrent[2];
	read_device(&hi2c1, DS2782_ADDRESS, &addr, 2, rawCurrent);
	uint16_t avCurrent = (rawCurrent[0] << 8) | (rawCurrent[1]);
	//avCurrent = avCurrent << 1;
	// convert from 2s complement form
	const int negative = (avCurrent & (1 << 15)) != 0;
	int nativeInt;

	if (negative) {
	  //nativeInt = (avCurrent | ~((1 << 16) - 1));
		avCurrent = (~avCurrent) + 1;
		nativeInt = avCurrent * -1;
		//nativeInt = (~(avCurrent - 0x01)) * -1;
	} else {
	  nativeInt = avCurrent;
	}

	nativeInt = nativeInt*10000/128;
	if (nativeInt > 9999) {
		sensor_data->avCurrent = 0000;
	} else {
		sensor_data->avCurrent = nativeInt;
	}
	PRINTF("Av Current: %d uA\n\r", sensor_data->avCurrent);

	DelayMs(1);

	uint16_t tankLevel_cm = 0;

	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_RESET);
	DelayMs(100);

	for (i = 0; i < 5; i++) {

		if (i == 0) {
			Read_Distance();
		} else {
			tankLevel_cm += Read_Distance();
		}
	}

	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_SET);
	sensor_data->tankLevelMeasure = tankLevel_cm*2/8;
	PRINTF("Water: %d cm\n\r", tankLevel_cm*2/8);

}

/*!
 * \brief Function that will send node and sensor data to base station.
*/
void Send_Data( NodeData_t * node_data, SensorData_t * sensor_data ) {

	char packet[50];
	uint8_t loraPacket[50];

	/*
	 * Packet Format
	 *
	 * dest|src|water|temp|voltage|current|
	 *
	 * 0x00|0x01|W100|T25.22|V4100|C-2345|
	 *
	 *  F  |  F |  F |   F  |  F  |  V   |
	 *
	 *  F = Fixed Length, V = Varied length
	 */

	sprintf(packet, "0x00|%s|W%03d|T%.2f|V%04d|C%d|", NODE_ID,
			sensor_data->tankLevelMeasure,
			sensor_data->temperature,
			sensor_data->batteryVoltage,
			(int) sensor_data->avCurrent);

	//sprintf(packet, "0x00|%s|W%03d", NODE_ID,
		//		sensor_data->tankLevelMeasure);

	memcpy(loraPacket, packet, strlen(packet)+1);

	PRINTF("Sending Packet: %s \n\r", loraPacket);

	State = NODE_DATA_SENDING;

	Radio.Send( loraPacket, sizeof(loraPacket)/sizeof(uint8_t)+1 );

	while(State == NODE_DATA_SENDING);


}

/*!
 * \brief Init the radio module.
*/
void LoraRadio_Init( void ) {

	// Radio initialization
	RadioEvents.TxDone = OnTxDone;
	RadioEvents.RxDone = OnRxDone;
	RadioEvents.TxTimeout = OnTxTimeout;
	RadioEvents.RxTimeout = OnRxTimeout;
	RadioEvents.RxError = OnRxError;

	Radio.Init( &RadioEvents );

	Radio.SetChannel( RF_FREQUENCY );

	Radio.SetTxConfig( MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
	LORA_SPREADING_FACTOR, LORA_CODINGRATE,
	  LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
	  true, 0, 0, LORA_IQ_INVERSION_ON, 3000000 );

	Radio.SetRxConfig( MODEM_LORA, LORA_BANDWIDTH, LORA_SPREADING_FACTOR,
	  LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
	  LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_ON,
	  0, true, 0, 0, LORA_IQ_INVERSION_ON, true );

	Radio.SetMaxPayloadLength(1, 60);

}

/*!
 * \brief In this function, the state of the GPIOx_MODER registers are
*               save into global variables GPIOA_MODER, GPIOB_MODER, and
*               GPIOC_MODER. The MODER registers are all then configured to
*               analog mode
*/
void Idd_SaveContext( void )
{
	char was_waiting = 0;

	// disable interrupts if they weren't already disabled
	if ( __get_PRIMASK() )
	{
	was_waiting = 1;
	}
	else
	{
	__disable_irq();
	}

	// Enable GPIO clocks
	RCC->IOPENR |= RCC_IOPENR_GPIOAEN | RCC_IOPENR_GPIOBEN | RCC_IOPENR_GPIOCEN;

	GPIOA_MODER = GPIOA->MODER;  // dummy read

	// Save the current mode of the I/O pins
	GPIOA_MODER = GPIOA->MODER;
	GPIOB_MODER = GPIOB->MODER;
	GPIOC_MODER = GPIOC->MODER;

	   // Configure GPIO port pins in Analog Input mode
	GPIOA->MODER = 0xFFFFFFFF;
	GPIOB->MODER = 0xFFFFFFFF;
	GPIOC->MODER = 0xFFFFFFFF;

	// Leave the external interrupts alone!
	GPIOC->MODER &= ~( GPIO_MODER_MODE13 ); // Input mode
	GPIOA->MODER &= ~( GPIO_MODER_MODE0 );

	// Disable GPIO clocks
	RCC->IOPENR &= ~( RCC_IOPENR_GPIOAEN | RCC_IOPENR_GPIOBEN | RCC_IOPENR_GPIOCEN );

	// enable interrupts if they were enabled before this function was called
	if ( !was_waiting )
	{
	__enable_irq();
	}
}

/*!
 * \brief Function to be restore GPIO registers
 */
void Idd_RestoreContext(void)
{
	char was_waiting = 0;

	// disable interrupts if they weren't already disabled
	if ( __get_PRIMASK() )
	{
	was_waiting = 1;
	}
	else
	{
	__disable_irq();
	}

	// Enable GPIO clocks
	RCC->IOPENR |= RCC_IOPENR_GPIOAEN | RCC_IOPENR_GPIOBEN | RCC_IOPENR_GPIOCEN;

	GPIOA->MODER = GPIOA_MODER; // dummy write

	// Restore the previous mode of the I/O pins
	GPIOA->MODER = GPIOA_MODER;
	GPIOB->MODER = GPIOB_MODER;
	GPIOC->MODER = GPIOC_MODER;

	// enable interrupts if they were enabled before this function was called
	if ( !was_waiting )
	{
	__enable_irq();
	}
}

/*!
 * \brief Function to be set MCU into Stop mode. Woken up by RTC alarm.
 */
void Enter_Stop( void )
{
	//Stop Sensors

	PRINTF("Sleeping sensors and stopping MCU\n\n\r");

	uint8_t data[3];
	data[0] = 0x01;
	data[1] = 0x01;
	data[2] = 0x18;
	write_device(&hi2c1, MCP9808_ADDRESS, data, 3);

	//GPIO_InitTypeDef GPIO_InitStructure;

	//GPIO_InitStructure.Mode = GPIO_MODE_ANALOG;
	//GPIO_InitStructure.Pull = GPIO_NOPULL;
	//GPIO_InitStructure.Pin = (GPIO_PIN_13 | GPIO_PIN_14);
	//HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);

	__HAL_RCC_DBGMCU_CLK_ENABLE();
	HAL_DBGMCU_DisableDBGStopMode();
	__HAL_RCC_DBGMCU_CLK_DISABLE();

	 // Power down TCXO
	 //HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12 , GPIO_PIN_RESET);
	 //Idd_SaveContext();

	 //HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);

	// Idd_RestoreContext();

/* Enable Clocks */
	 RCC->APB1ENR |= RCC_APB1ENR_PWREN;
	 RCC->IOPENR |= RCC_IOPENR_GPIOAEN;

   /* Prepare to enter stop mode */
	 PWR->CR |= PWR_CR_CWUF;      // clear the WUF flag after 2 clock cycles
	 PWR->CR &= ~( PWR_CR_PDDS ); // Enter stop mode when the CPU enters deepsleep
   // V_REFINT startup time ignored | V_REFINT off in LP mode | regulator in LP mode
	 PWR->CR |= PWR_CR_FWU | PWR_CR_ULP | PWR_CR_LPSDSR;
	 RCC->CFGR |= RCC_CFGR_STOPWUCK; // HSI16 oscillator is wake-up from stop clock
	 SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk; // low-power mode = stop mode

	 __disable_irq();

	 Idd_SaveContext();
	 I2C1->CR1 &= ~I2C_CR1_PE;  // Address issue 2.5.1 in Errata
	 __WFI(); // enter low-power mode
	 wakeTime = HW_RTC_GetTimerValue();
	 I2C1->CR1 |= I2C_CR1_PE;
	 Idd_RestoreContext();

	 __enable_irq(); // <-- go to isr
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void stm32l0_SystemClock_Config(void)
{

	RCC_OscInitTypeDef RCC_OscInitStruct;
	  RCC_ClkInitTypeDef RCC_ClkInitStruct;
	  RCC_PeriphCLKInitTypeDef PeriphClkInit;

	    /**Configure the main internal regulator output voltage
	    */
	  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

	    /**Initializes the CPU, AHB and APB busses clocks
	    */
	  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
	  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	  RCC_OscInitStruct.HSICalibrationValue = 16;
	  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
	  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
	  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	  {

	  }

	    /**Initializes the CPU, AHB and APB busses clocks
	    */
	  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
	                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
	  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
	  {

	  }

	  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_I2C1;
	  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
	  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
	  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	  {

	  }

	    /**Configure the Systick interrupt time
	    */
	  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

	    /**Configure the Systick
	    */
	  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	  /* SysTick_IRQn interrupt configuration */
	  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/*!
 * \brief Function to be set MCU into Low Power run mode
 */
void Enter_LPRun( void )
{
    /* 1. Each digital IP clock must be enabled or disabled by using the
                   RCC_APBxENR and RCC_AHBENR registers */
    RCC->APB1ENR |= RCC_APB1ENR_PWREN;
    /* 2. The frequency of the system clock must be decreased to not exceed the
          frequency of f_MSI range1. */

    stm32l0_SystemClock_Config();
    /* 3. The regulator is forced in low-power mode by software
          (LPRUN and LPSDSR bits set ) */
    PWR->CR &= ~PWR_CR_LPRUN; // Be sure LPRUN is cleared!

    PWR->CR |= PWR_CR_LPSDSR; // must be set before LPRUN
    PWR->CR |= PWR_CR_LPRUN; // enter low power run mode

 //   HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12 , GPIO_PIN_SET);

    PRINTF("Woken! (Node ID: %s)\n\r", NODE_ID);
}

/*!
 * \brief Function to be executed on Radio Tx Done event
 */
void OnTxDone( void )
{
   // Radio.Sleep( );
    //State = TX;
    State = NODE_DATA_COMPLETE;
}

/*!
 * \brief Function to be executed on Radio Rx Done event
 */
void OnRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
    //Radio.Sleep( );
    BufferSize = size;
    memcpy( Buffer, payload, BufferSize );
    RssiValue = rssi;
    SnrValue = snr;
    //State = RX;
  
    //PRINTF("RssiValue=%d dBm, SnrValue=%d\n\r", rssi, snr);
    State = NODE_DATA_RECV;
}

/*!
 * \brief Function executed on Radio Tx Timeout event
 */
void OnTxTimeout( void )
{
   // Radio.Sleep( );
    //State = TX_TIMEOUT;
  
    PRINTF("OnTxTimeout\n\r");
}

/*!
 * \brief Function executed on Radio Rx Timeout event
 */
void OnRxTimeout( void )
{
    //Radio.Sleep( );
    State = NODE_DATA_TIMEOUT;
}

/*!
 * \brief Function executed on Radio Rx Error event
 */
void OnRxError( void )
{
    //Radio.Sleep( );
    State = NODE_DATA_RECV_ERR;
    PRINTF("OnRxError\n\r");
}





