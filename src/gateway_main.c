/*
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
    (C)2013 Semtech

Description: Ping-Pong implementation

License: Revised BSD License, see LICENSE.TXT file include in the project

Maintainer: Miguel Luis and Gregory Cristian
*/
/******************************************************************************
  * @file    main.c
  * @author  MCD Application Team
  * @version V1.1.4
  * @date    08-January-2018
  * @brief   this is the main!
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Packet Structure
 *
 * #|dest address (node)|source address|payload|#
 *
 *
 */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include <data_structs.h>
#include <string.h>
#include "hw.h"
#include "radio.h"
#include "timeServer.h"
#include "delay.h"
#include "low_power_manager.h"
#include "vcom.h"
#include "stm32l0xx_hal_rtc.h"
#include "SPBTLE_RF.h"
#include "eeprom.h"


#include "osal.h"
#include "sample_service.h"
#include "role_type.h"
#include "debug.h"
#include "stm32_bluenrg_ble.h"
#include "bluenrg_utils.h"


#define RF_FREQUENCY                                915000000 // Hz

#define TX_OUTPUT_POWER                             14        // dBm


#define LORA_BANDWIDTH                              0         // [0: 125 kHz,
                                                              //  1: 250 kHz,
                                                              //  2: 500 kHz,
                                                              //  3: Reserved]
#define LORA_SPREADING_FACTOR                       7         // [SF7..SF12]
#define LORA_CODINGRATE                             1         // [1: 4/5,
                                                              //  2: 4/6,
                                                        	  //  3: 4/7,
                                                              //  4: 4/8]
#define LORA_PREAMBLE_LENGTH                        8         // Same for Tx and Rx
#define LORA_SYMBOL_TIMEOUT                         5         // Symbols
#define LORA_FIX_LENGTH_PAYLOAD_ON                  false
#define LORA_IQ_INVERSION_ON                        false

uint32_t RX_TIMEOUT_VALUE = 40000;
#define BUFFER_SIZE                	64 // Define the payload size here
#define CLOCK_SECOND               	1000
#define CLOCK_MINUTE              	60*CLOCK_SECOND

#define PRESSURE 					0x800
#define uS_10 						490

#define GATEWAY_ID "0x00"

#define TANK_ERROR					0x01
#define NODE_NO_RESPOND				0x02


/*
 * SETTINGS TO CHANGE FOR PROGRAMMING
 */

#define INTERVAL_TIME 				5*CLOCK_MINUTE
#define TOTAL_NODES					4
#define REPROGRAM 					0

#define NO_SENSOR_ERROR 			0
#define SEND_STATUS_MESSAGE			1
uint8_t SEND_ERROR_MESSAGE = 1;



BLE_RoleTypeDef BLE_Role = SERVER;

IWDG_HandleTypeDef hiwdg;


volatile int ble_wakeup_flag = FALSE;

volatile int txt_wakeup_flag = FALSE;


static TimerEvent_t BLETimer;
static TimerEvent_t TxtTimer;

static volatile int noUserFlag = FALSE;
extern volatile uint8_t messageRecv;
extern volatile int connected;
extern volatile messageType_t BLEUserMesg;

extern volatile uint16_t tankNumber;
extern volatile uint32_t tankHighReading;
extern volatile uint32_t tankLowReading;
extern volatile char phNum[12];
extern volatile uint8_t priority;


volatile currentStatus_t State;

uint32_t GPIOA_MODER, GPIOB_MODER, GPIOC_MODER;
uint8_t bnrg_expansion_board = 0;

RTC_HandleTypeDef hrtc;

uint16_t BufferSize = BUFFER_SIZE;
uint8_t Buffer[BUFFER_SIZE];

int8_t RssiValue = 0;
int8_t SnrValue = 0;

uint8_t *flag = 0;

uint8_t sizeSend = 0;

uint32_t finsihedBLESample = 0;
uint8_t getMesg = 0;

volatile uint16_t send = 0, bufferCount = 0, recvFinished = FALSE, txFinished = TRUE;
char usartToSend[350];
char usartReceived[150];
char textMesg[150];
volatile int sendingMessage = 0;
volatile int Linecount = 0;
volatile int recvMessage = 0;
volatile int16_t rssiValue = 0;


typedef enum
{
OKAY,
USART,
NOTHING

}ErrorStatus_t;

volatile ErrorStatus_t ERROR_CODE;

 /* Led Timers objects*/
//static  TimerEvent_t timerLed;

/* Private function prototypes -----------------------------------------------*/

/*!
 * Radio events function pointer
 */
static RadioEvents_t RadioEvents;

void OnTxDone( void );
void OnRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );
void OnTxTimeout( void );
void OnRxTimeout( void );
void OnRxError( void );
void Enter_LPRun( void );
void Enter_Stop( void );
void LoraRadio_Init( void );
void Gateway_Init( GatewayData_t * gateway_data , SensorData_t * sensor_data);
static void MX_RTC_Init(void);
void ble_wakeup( void );
void txt_wakeup( void );
void no_user ( void );
void send_text_message( uint32_t errorFlag, GatewayData_t * gateway_data,
		SensorData_t * sensor_data , char *manualNumber, uint32_t batteryFlag);
void manage_text_message( GatewayData_t * gateway_data, SensorData_t * sensor_data );
void USART1_IRQHandler(void);
void send_command_list( void );
void ble_init(void);
void start_ble_connection( uint32_t timeDiff, uint32_t sleepTime, GatewayData_t * gateway_data, SensorData_t * sensor_data );
void parseBLEMesg( messageType_t bleCommand, GatewayData_t * gateway_data, SensorData_t * sensor_data);
void bad_text ( void );
uint32_t check_battery( GatewayData_t * gateway_data,
		SensorData_t * sensor_data, uint16_t totalNodes );

/**
 * Main application entry point.
 */
int main( void )
{


  HAL_Init( );
  
  Enter_LPRun();

  DBG_Init( );

  HW_Init( );

  DelayMs(1000);

  LoraRadio_Init();

  GatewayData_t gateway_data;
  SensorData_t sensor_data;

  Gateway_Init(&gateway_data, &sensor_data);

  init_eeprom();

  //add_eeprom_num(0, "0439222326");
  add_eeprom_num(0, "0408711061");
  add_eeprom_num(1, "0438711061");

  // tank 1 - raw
  updateTankLevels(&sensor_data, 1, 48, 150, 50);
  //updateTankLevels(&sensor_data, 1, 1, 600, -80);
  //updateTankLevels(&sensor_data, 1, 1, 40, 20);

  // tank 2 - UF
  updateTankLevels(&sensor_data, 2, 72, 270, 60);
  //updateTankLevels(&sensor_data, 2, 1, 600, -80);
  //updateTankLevels(&sensor_data, 2, 1, 40, 20);

  // tank 3 - black
  updateTankLevels(&sensor_data, 3, 26, 225, 20);
  //updateTankLevels(&sensor_data, 3, 1, 600, -80);
  //updateTankLevels(&sensor_data, 3, 1, 40, 20);

  // tank 4 - reserve
  updateTankLevels(&sensor_data, 4, 72, 270, 90);
  //updateTankLevels(&sensor_data, 4, 1, 600, -80);
  //updateTankLevels(&sensor_data, 4, 1, 40, 20);

  // tank 5 - big tank
  //updateTankLevels(&sensor_data, 4, 30, 200, 20);


  // tank 6 - sand filter tank - opposite
  //updateTankLevels(&sensor_data, 4, 30, 200, 70);

  ble_init();

  MX_RTC_Init();

  uint32_t sleepTime = 0, wakeTimer = 0;

  uint32_t lastErrorFlag = 0;

  txt_wakeup_flag = FALSE;

  uint32_t RTC_wake_flag = TRUE;

  RX_TIMEOUT_VALUE = gateway_data.reportInterval+CLOCK_MINUTE;

  int32_t sleepForMs = 0;

  while( 1 ) {

	  LED_On(LED_RED1);

	  if ((RTC_wake_flag == TRUE && ble_wakeup_flag == FALSE && txt_wakeup_flag == FALSE) | get_wakeup_status()) {
		  clear_wakeup_status();
		  get_node_data(&gateway_data, &sensor_data);
		  uint32_t errorFlag = check_readings(&gateway_data, &sensor_data, gateway_data.totalNodes);
		  uint32_t batteryFlag = check_battery(&gateway_data, &sensor_data, gateway_data.totalNodes);

		  /*
		  Vsamples[samplesCnt] = sensor_data.battVoltageReadings[1];
		  Csamples[samplesCnt] = sensor_data.averageCurrentReadings[1];

		  for (int i = 0; i < (samplesCnt+1); i++) {
			  PRINTF("%d,", Vsamples[i]);
			  if (i % 34 == 0) {
				  PRINTF("\n\r");
			  }
		  }
		  PRINTF("\n\r");

		  for (int i = 0; i < (samplesCnt+1); i++) {
			  PRINTF("%d,", Csamples[i]);
			  if (i % 34 == 0) {
				  PRINTF("\n\r");
			  }
		  }
		  PRINTF("\n\r");

		  samplesCnt++;
		  */

		  if (errorFlag != NO_SENSOR_ERROR) {
			  // We have an error on one or more tanks!
			  gateway_data.sleepTime += 2;
			  if (gateway_data.userAck) {
				  if (errorFlag != lastErrorFlag) {
					  // Send text message with error
					  if (get_eeprom_num_count() != 0) {
						  send_text_message(errorFlag, &gateway_data, &sensor_data, NULL, batteryFlag);
						  gateway_data.lastMessageSent = TANK_ERROR;
					  }
				  }
			  } else {
			  	  // Send text message with error
				  if (get_eeprom_num_count() != 0) {
					  send_text_message(errorFlag, &gateway_data, &sensor_data, NULL, batteryFlag);
					  gateway_data.lastMessageSent = TANK_ERROR;
					  gateway_data.currentUser++;
				  	  gateway_data.currentUser %= get_eeprom_num_count();
				  }
			  }
			  lastErrorFlag = errorFlag;
		  } else {
			  gateway_data.currentUser = 0;
		  }

		  Radio.Sleep();
		  sleepTime = HW_RTC_GetTimerSeconds();

		  sleepForMs = gateway_data.sleepInterval -
				  (HW_RTC_GetTimerSeconds() - gateway_data.sleepTime)*CLOCK_SECOND;
				 // (sleepTime - wakeTime)*CLOCK_SECOND - timeOutSleep;

		  PRINTF("Prelim Sleeping for: %d\n\r", sleepForMs);
		  if ((sleepForMs <= 0) || (sleepForMs > (gateway_data.sleepInterval + 1*CLOCK_MINUTE))) {
			  sleepForMs = 1*CLOCK_MINUTE;
			  RX_TIMEOUT_VALUE = gateway_data.sleepInterval;
			  gateway_data.ignoreRxTime = 1;
		  } else {
			  RX_TIMEOUT_VALUE = 40000;
			  gateway_data.ignoreRxTime = 0;
		  }

		  PRINTF("Actual Sleeping for: %d\n\r", sleepForMs);

		  HW_RTC_SetAlarm(HW_RTC_ms2Tick(sleepForMs));

	  } else {
		  if (gateway_data.bleForcedSample) {
			  sleepTime = HW_RTC_GetTimerSeconds();
		  } else if (ble_wakeup_flag) {
			  RX_TIMEOUT_VALUE = gateway_data.sleepInterval;
			  ble_wakeup_flag = FALSE;
			  txt_wakeup_flag = FALSE;
			  gateway_data.ignoreRxTime = 1;
			  continue;
		  }
	  }
	  // If a text is recv during a sampling period
	  if (txt_wakeup_flag & RTC_wake_flag) {
		  manage_text_message(&gateway_data, &sensor_data);
	  }
	  // If a BLE request is recv during a sampling period
	  if (ble_wakeup_flag & RTC_wake_flag) {
		  start_ble_connection(5,
		  					  sleepForMs,
		  					  &gateway_data, &sensor_data);
	  }
	  ble_wakeup_flag = FALSE;
	  txt_wakeup_flag = FALSE;
	  RTC_wake_flag = FALSE;


	 // Sleep and Low power mode
	  LED_Off(LED_RED1);
	  PRINTF("Sleep: %d\n\r", HW_RTC_GetTimerSeconds());
	  Enter_Stop();
	  Enter_LPRun();
	  PRINTF("Wake: %d\n\r", HW_RTC_GetTimerSeconds());
	  wakeTimer = HW_RTC_GetTimerSeconds();
	  gateway_data.bleForcedSample = FALSE;


	  // Check wake-up source (EXTI or RTC Alarm)
	  if (PWR->CSR == 40) {
		  // BLE
		  if (ble_wakeup_flag) {
			  int count = 0;
			  while (HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_2) == GPIO_PIN_RESET) {
				  count++;
				  if (count >= 500) {
					  sensor_data.pressure = 0;
					  // low mains pressure!
					  // Send text message with error
					  if (get_eeprom_num_count() != 0) {
						  send_text_message(PRESSURE, &gateway_data, &sensor_data, NULL, 0);
						  //gateway_data.currentUser++;
						  //gateway_data.currentUser %= get_eeprom_num_count();

					  }
				  }
				  DelayMs(10);
			  }
			  // if there is no pressure error
			  if (sensor_data.pressure) {
				  PRINTF("BLE\n\r");
				  LED_On(LED_RED1);
				  wakeTimer = HW_RTC_GetTimerSeconds();
				  start_ble_connection((wakeTimer - sleepTime + 5),
						  sleepForMs,
						  &gateway_data, &sensor_data);
			  }
		  }
		  // TXT
		  else if (txt_wakeup_flag) {
			  // respond to message
			  PRINTF("TXT\n\r");
			  wakeTimer = HW_RTC_GetTimerSeconds();
			  manage_text_message(&gateway_data, &sensor_data);
		  }

	  } else {
		  PRINTF("RTC\n\r");
		  ble_wakeup_flag = FALSE;
		  RX_TIMEOUT_VALUE = 30*CLOCK_SECOND;
		  RTC_wake_flag = TRUE;
		  // RTC
		  // continue normal loop
	  }
  }
}

/**
 * Function that will wait for a response from the txt module
 */
ErrorStatus_t wait_response( void ) {

	ERROR_CODE = USART;
	recvFinished = FALSE;
	txFinished = FALSE;

	uint8_t errorCount = 0;

	while (ERROR_CODE == USART) {

		int timeout = 0;
		USART1->TDR = usartToSend[send++];
		PRINTF("Waiting TxFinished!\n\r");
		while (txFinished == FALSE) {
			timeout++;
			if (timeout == 100) {
				PRINTF("TIMEOUT on tx\n\r");
				return USART;
			}
			DelayMs(10);
		}
		timeout = 0;
		if (Linecount >= 0) {
			Linecount = 0;
		}
		PRINTF("Waiting RecvFinished!\n\r");
		while (recvFinished == FALSE) {
			timeout++;
			if (timeout == 200) {
				PRINTF("TIMEOUT on recv\n\r");
				return USART;
			}
			DelayMs(10);
		}
		recvFinished = FALSE;

		send = 0;
		if (errorCount == 2) {
			PRINTF("Tried 3 times, Couldn't get 'OK' response from TXT\n\r");
			PRINTF("Command: %s\n\r", usartToSend);
			return USART;
		}
		errorCount++;
	}

	if (ERROR_CODE == OKAY) {
		//PRINTF("Successfully sent command: %s", usartToSend);
	}
	return OKAY;
}

void send3Gsms( char *txt, char *numberToSend, GatewayData_t * gateway_data) {

		char *message = malloc(sizeof(char) * 200);

		strcpy(message, txt);

		// add ctrl-Z to finish message
		sprintf(message, "%s%c", message, (char) 26);

		//PRINTF("SENDING TXT_MESSAGE: ' %s '\n\r", usartCommand);

		uint8_t doWeSend = 0;

		if (numberToSend == NULL) {
		// Read number at index gateway_data->currentUser in eeprom
			char* phNum = malloc(sizeof(char) * 11);
			get_eeprom_num(gateway_data->currentUser, &phNum);
			phNum[10] = '\0';
			sprintf(usartToSend, "AT+CMGS=\"%s\"\r\n", phNum);
			PRINTF("Sending to: %s\n\r", phNum);
			free(phNum);
			doWeSend = SEND_ERROR_MESSAGE;
		} else {
			sprintf(usartToSend, "AT+CMGS=\"%s\"\r\n", numberToSend);
			PRINTF("Sending to manual: %s\n\r", numberToSend);
			doWeSend = SEND_STATUS_MESSAGE;
		}


		if (doWeSend) {

			sendingMessage = 0;

			txFinished = FALSE;
			send = 0;
			USART1->TDR = usartToSend[send++];
			int timeout = 0;
			while (txFinished == FALSE) {
				timeout++;
				if (timeout == 300) {
					PRINTF("TIMEOUT on TX text\n\r");
					break;
				}
				DelayMs(10);
			}
			// wait for txt to respond, ready for message data
			timeout = 0;
			while (sendingMessage == 0) {
				timeout++;
				if (timeout == 300) {
					PRINTF("TIMEOUT on sending Mesg\n\r");
					break;
				}
				DelayMs(10);
			}

			// now send 'message'!
			txFinished = FALSE;
			send = 0;
			strcpy(usartToSend, message);
			wait_response();

			sendingMessage = 0;
		}

		free(message);
}


/**
 * Function that will send a text message.
 * Will call HAL functions to use the USART
 */
void send_text_message( uint32_t errorFlag, GatewayData_t * gateway_data,
		SensorData_t * sensor_data , char *manualNumber, uint32_t batteryFlag) {

	/*
	 * Error: Tank/s 1,2
	 *
	 *	Tank 1: 20%
	 *	Tank 2: 24%
	 *	Tank 3: 80%
	 *	Tank 4: 90%
	 *
	 */
	char *usartCommand = malloc(sizeof(char)*200);
	int bit = 1;
	char tank[15];

	if (errorFlag == PRESSURE) {

		strcpy(usartCommand, "Error: Low Pressure!\n\n");

	} else if (errorFlag != NO_SENSOR_ERROR) {
		// have an error
		if (errorFlag > PRESSURE) {
			strcpy(usartCommand, "Error: Pressure and Tank/s  ");
		} else {
			strcpy(usartCommand, "Error: Tank/s  ");
		}
		bit = 1;

		for (int i = 1; i <= gateway_data->totalNodes; i++) {
			if ((errorFlag & bit) == bit) {
				strcat(usartCommand, itoa(i, tank, 10));
				if (errorFlag > ((bit << 1)-1)) {
					strcat(usartCommand, ",");
				}
			}
			bit*=2;
		}
		strcat(usartCommand, "\n\n");

	} else {
		strcpy(usartCommand, "Sensor Update\n\n");
	}

	for (int i = 0; i < gateway_data->totalNodes; i++) {

		sprintf(usartCommand, "%sTank %d: W=%d%%, T=%.2f*C\n", usartCommand, i+1,
				(int) (sensor_data->tankInters[i].gradient*sensor_data->tankReadings[i] +
												sensor_data->tankInters[i].intercept),
				sensor_data->temperatureReadings[i]);
	}
	if (sensor_data->pressure) {
		strcat(usartCommand, "Pressure: Okay\n");
	} else {
		strcat(usartCommand, "Pressure: Low\n");
	}
	bit = 1;
	if (batteryFlag != NO_SENSOR_ERROR) {
		strcat(usartCommand, "\n");
		strcat(usartCommand, "Batt Low: ");
		for (int i = 1; i <= gateway_data->totalNodes; i++) {
			if ((batteryFlag & bit) == bit) {
				strcat(usartCommand, itoa(i, tank, 10));
				if (errorFlag > bit) {
					strcat(usartCommand, ",");
				}
			}
			bit*=2;
		}
	}

	send3Gsms(usartCommand, manualNumber, gateway_data);

	free(usartCommand);


}

/**
 * Parses the recv text message.
 * Two main options:
 * 1. Send an status text message back to sender
 * 2. Treat the message as an user sending 'Acknowledge'
 */
void manage_text_message(GatewayData_t * gateway_data, SensorData_t * sensor_data ) {


	// if message is 'status || Status', call 'send_text_message(NO_SENSOR_ERROR,...)
	// else if message is 'yes || Yes', gateway_data->userAck = 1;

	// read message

	PRINTF("Manage Text!\n\r");
	DelayMs(500);
	send = 0;
	getMesg = 1;
	//sprintf(usartCommand, "AT+CMGR=1\r\n");
	//sizeSend = 11;

	#define STATUS 1
	#define YES 2
	#define THRESUPDATE 3
	#define PHUPDATE 4

	uint8_t mesg = 0;

	PRINTF("Sending Read\n\r");

	strcpy(usartToSend, "AT+CMGR=1\r\n");
	sizeSend = 11;

	PRINTF("Waiting Response!\n\r");

	if (wait_response() == OKAY) {
		// read buffer

		int mesgIndex = 0, qMarkCount = 0;

		for (mesgIndex = 10; mesgIndex < strlen(textMesg); mesgIndex++) {

			if (textMesg[mesgIndex] == '"') {
				qMarkCount++;
			}
			if (qMarkCount == 7) {
				mesgIndex = mesgIndex+2;
				break;
			}

		}
		PRINTF("Textmesg: %c, %c \n\r", textMesg[mesgIndex],
				textMesg[mesgIndex+1]);

		PRINTF("TextMessage: %s\n\r", textMesg);

		if ((textMesg[mesgIndex] == 'S') && (textMesg[mesgIndex+1] == 't') &&
				(textMesg[mesgIndex+2] == 'a') && (textMesg[mesgIndex+3] == 't') &&
				(textMesg[mesgIndex+4] == 'u') && (textMesg[mesgIndex+5] == 's')) {

			mesg = STATUS;

		} else if ((textMesg[mesgIndex] == 'Y') && (textMesg[mesgIndex+1] == 'e') &&
			(textMesg[mesgIndex+2] == 's')) {


			mesg = YES;

		} else if ((textMesg[mesgIndex] == 'T') && (textMesg[mesgIndex+1] == 'a') &&
				(textMesg[mesgIndex+2] == 'n') && (textMesg[mesgIndex+3] == 'k')) {
			PRINTF("TankLevel update\n\r");
			char low[5];
			char high[5];
			char thres[5];

			memcpy( high , &textMesg[mesgIndex+7], 3 );
			memcpy( low , &textMesg[mesgIndex+11], 3 );
			memcpy( thres , &textMesg[mesgIndex+15], 3 );

			updateTankLevels(sensor_data, (int)textMesg[mesgIndex+5]-'0',
					atoi(high), atoi(low), atoi(thres));

			mesg = STATUS;

		} else if ((textMesg[mesgIndex] == 'T') && (textMesg[mesgIndex+1] == 'h') &&
				(textMesg[mesgIndex+2] == 'r') && (textMesg[mesgIndex+3] == 'e')
				&& (textMesg[mesgIndex+4] == 's')) {

			int tankNum = (int)textMesg[mesgIndex+6]-'0';
			char percent[5];

			memcpy( percent , &textMesg[mesgIndex+8], 3 );

			sensor_data->tankThresholds[tankNum - 1] = ((-1*sensor_data->tankInters[tankNum - 1].intercept) + atoi(percent))/
										sensor_data->tankInters[tankNum - 1].gradient;
			mesg = THRESUPDATE;


		} else if ((textMesg[mesgIndex] == 'P') && (textMesg[mesgIndex+1] == 'h') &&
				(textMesg[mesgIndex+2] == 'o') && (textMesg[mesgIndex+3] == 'n')
				&& (textMesg[mesgIndex+4] == 'e')) {

			int priority = (int)textMesg[mesgIndex+6]-'0';
			char phone[11];

			memcpy( phone , &textMesg[mesgIndex+8], 10 );

			phone[10] = '\0';

			add_eeprom_num(priority-1, phone);

			mesg = PHUPDATE;


		} else if ((textMesg[mesgIndex] == 'S') && (textMesg[mesgIndex+1] == 't') &&
				(textMesg[mesgIndex+2] == 'o') && (textMesg[mesgIndex+3] == 'p')) {

			SEND_ERROR_MESSAGE = 0;

		} else if ((textMesg[mesgIndex] == 'S') && (textMesg[mesgIndex+1] == 't') &&
				(textMesg[mesgIndex+2] == 'a') && (textMesg[mesgIndex+3] == 'r')
				&& (textMesg[mesgIndex+4] == 't')) {

			SEND_ERROR_MESSAGE = 1;
		} else if ((textMesg[mesgIndex] == 'S') && (textMesg[mesgIndex+1] == 'k') &&
				(textMesg[mesgIndex+2] == 'i') && (textMesg[mesgIndex+3] == 'p')) {

			sensor_data->skipTank = (int)textMesg[mesgIndex+5]-'0';

		}
		int startIndex = 25;

		for (startIndex = 25; startIndex < strlen(textMesg); startIndex++) {

			if (textMesg[startIndex] == '+') {
				break;
			}

		}
		char number[10];

		memcpy(number, &textMesg[startIndex+2], 10);
		number[0] = '0';

		if (mesg == STATUS) {
			send_text_message(NO_SENSOR_ERROR, gateway_data, sensor_data, number, 0);
		} else if (mesg == YES) {
			if (gateway_data->lastMessageSent == TANK_ERROR) {
				gateway_data->userAck = 1;
			} else if (gateway_data->lastMessageSent == NODE_NO_RESPOND) {
				gateway_data->nodeNoResponseAck = 1;
			}
		} else if (mesg == THRESUPDATE) {
			send3Gsms("Threshold Updated!", number, gateway_data);
		} else if (mesg == PHUPDATE) {
			send3Gsms("Phone Updated!", number, gateway_data);
		}

	}

	// delete message
	strcpy(usartToSend, "AT+CMGD=1,4\r\n");
	sizeSend = 13;
	if (wait_response() != OKAY) {
		PRINTF("Failed to set delete txt\n\r");
		return;
	}

	strcpy(textMesg, " ");

	getMesg = 0;
}


/* Init the gateway pins */
void Gateway_Init( GatewayData_t * gateway_data , SensorData_t * sensor_data)
{

	gateway_data->totalNodes = TOTAL_NODES;
	gateway_data->reportInterval = INTERVAL_TIME;
	gateway_data->sleepInterval = gateway_data->reportInterval - CLOCK_SECOND*5;
	gateway_data->sleepTime = 0;

	gateway_data->userAck = 0;
	gateway_data->currentUser = 0;
	gateway_data->updateReport = 0;

	sensor_data->tankReadings = malloc(sizeof(uint16_t)*TOTAL_NODES);
	sensor_data->temperatureReadings = malloc(sizeof(double)*TOTAL_NODES);
	sensor_data->averageCurrentReadings = malloc(sizeof(uint32_t)*TOTAL_NODES);
	sensor_data->battVoltageReadings = malloc(sizeof(uint16_t)*TOTAL_NODES);
	sensor_data->tankInters = malloc(sizeof(TankInterpolation_t)*TOTAL_NODES);
	sensor_data->tankThresholds = malloc(sizeof(uint16_t)*TOTAL_NODES);
	sensor_data->tankHasError = malloc(sizeof(uint8_t)*TOTAL_NODES);
	sensor_data->didNotReport = malloc(sizeof(uint8_t)*TOTAL_NODES);
	sensor_data->didNotReportCnt = malloc(sizeof(uint8_t)*TOTAL_NODES);
	sensor_data->didNotReportTotal = malloc(sizeof(uint8_t)*TOTAL_NODES);
	sensor_data->pressure = 1;

	for (int i = 0; i < TOTAL_NODES; i++) {
		sensor_data->tankReadings[i] = 25; // 25cm
		sensor_data->temperatureReadings[i] = 25; // 25*C
		sensor_data->averageCurrentReadings[i] = 0;
		sensor_data->battVoltageReadings[i] = 0;
		sensor_data->tankInters[i].gradient = 1;
		sensor_data->tankInters[i].intercept = 1;
		sensor_data->tankThresholds[i] = 300;
		sensor_data->tankHasError[i] = 0;
		sensor_data->didNotReport[i] = 0;
		sensor_data->didNotReportCnt[i] = 0;
		sensor_data->didNotReportTotal[i] = 0;
	}


	GPIO_InitTypeDef GPIO_InitStructure;

	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();

	// Ultrasonic Sensor

	// Trigger pin
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Pin = GPIO_PIN_0;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStructure.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, 0);

	GPIO_InitTypeDef GPIO_InitStructure2;

	// Echo pin
	GPIO_InitStructure2.Mode = GPIO_MODE_INPUT;
	GPIO_InitStructure2.Pin = GPIO_PIN_13;
	GPIO_InitStructure2.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStructure2.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStructure2);

	// PB2 BLE IRQ
	GPIO_InitStructure2.Pin = GPIO_PIN_2;
	GPIO_InitStructure2.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStructure2.Pull = GPIO_PULLUP;
	GPIO_InitStructure2.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStructure2.Alternate = 0;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStructure2);

	HW_GPIO_SetIrq(GPIOB, GPIO_PIN_2, 2, &ble_wakeup);

	// PB12 TXT RI
	GPIO_InitStructure2.Pin = GPIO_PIN_12;
	GPIO_InitStructure2.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStructure2.Pull = GPIO_PULLUP;
	GPIO_InitStructure2.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStructure2.Alternate = 0;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStructure2);

	HW_GPIO_SetIrq(GPIOB, GPIO_PIN_12, 2, &txt_wakeup);


	// GPIO configuration for USART1 signals
	// Select AF mode (10) on PA9 and PA10
	// AF4 for USART1 signals
	GPIOA->MODER = (GPIOA->MODER & ~(GPIO_MODER_MODE9|GPIO_MODER_MODE10))\
	                 | (GPIO_MODER_MODE9_1 | GPIO_MODER_MODE10_1);
	GPIOA->AFR[1] = (GPIOA->AFR[1] &~ (0x00000FF0))\
	                  | (4 << (1 * 4)) | (4 << (2 * 4));

	// Enable the peripheral clock USART1
	RCC->APB2ENR |= RCC_APB2ENR_USART1EN;

	// Configure USART1
	// (1) oversampling by 16, 9600 baud
	// (3) 8 data bit, 1 start bit, 1 stop bit, no parity, reception and transmission enabled
	USART1->BRR = 160000 / 96;
	USART1->CR1 = USART_CR1_TE | USART_CR1_RXNEIE | USART_CR1_RE | USART_CR1_UE;

	int count = 0;
	while((USART1->ISR & USART_ISR_TC) != USART_ISR_TC)
	{
		if (count++ == 50000000) {
			PRINTF("Timeout on USART init\n\r");
			return;
		}
	}

	USART1->ICR = USART_ICR_TCCF;
	USART1->CR1 |= USART_CR1_TCIE;

	// Configure IT
	// Set priority for USART1_IRQn
	// Enable USART1_IRQn
	NVIC_SetPriority(USART1_IRQn, 0);
	NVIC_EnableIRQ(USART1_IRQn);

	LED_On(LED_RED2);
	DelayMs(1000);
	LED_Off(LED_RED2);
	DelayMs(8000);

	// Init txt module mode
	strcpy(usartToSend, "AT\r\n");
	sizeSend = 4;
	if (wait_response() != OKAY) {
		PRINTF("Failed to init AT\n\r");
		//return;
	}
	strcpy(usartToSend, "ATE0\r\n");
	sizeSend = 6;
	if (wait_response() != OKAY) {
		PRINTF("Failed to turn off echo\n\r");
		//return;
	}

	strcpy(usartToSend, "AT+CMGF=1\r\n");
	sizeSend = 11;
	if (wait_response() != OKAY) {
		PRINTF("Failed to set Text mode\n\r");
		return;
	}

	strcpy(usartToSend, "AT+CMGD=1,4\r\n");
	sizeSend = 13;
	if (wait_response() != OKAY) {
		PRINTF("Failed to set delete txt\n\r");
		return;
	}

	TimerInit(&TxtTimer, bad_text);
	TimerSetValue( &TxtTimer, CLOCK_SECOND*10);


}

/* IWDG init function
static void MX_IWDG_Init(void) {

    hiwdg.Instance = IWDG;
    hiwdg.Init.Prescaler = IWDG_PRESCALER_4;
    hiwdg.Init.Reload = 4095;

    if (HAL_IWDG_Init(&hiwdg) != HAL_OK) {
        Error_Handler();
    }

}
*/

/* RTC init function */
static void MX_RTC_Init(void)
{

  RTC_TimeTypeDef sTime;
  RTC_DateTypeDef sDate;

    /**Initialize RTC Only
    */
  hrtc.Instance = RTC;
if(HAL_RTCEx_BKUPRead(&hrtc, RTC_BKP_DR0) != 0x32F2){
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {

  }

    /**Initialize RTC and set the Time and Date
    */
  sTime.Hours = 0x0;
  sTime.Minutes = 0x0;
  sTime.Seconds = 0x0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
  {
  }

  sDate.WeekDay = RTC_WEEKDAY_MONDAY;
  sDate.Month = RTC_MONTH_JANUARY;
  sDate.Date = 0x1;
  sDate.Year = 0x0;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
  {
  }

    /**Enable Calibrartion
    */
  if (HAL_RTCEx_SetCalibrationOutPut(&hrtc, RTC_CALIBOUTPUT_512HZ) != HAL_OK)
  	  {
 	 }

    HAL_RTCEx_BKUPWrite(&hrtc,RTC_BKP_DR0,0x32F2);
  }

}

void updateTankLevels(SensorData_t *sensor_data, int tankNum,
		int highLevel, int lowLevel, int percent) {

	// Two points
	// (highLevel, 100), (lowLevel, 0)

	double gradientNumerator = 100 - 0;
	double gradientDenominator = highLevel- lowLevel;

	sensor_data->tankInters[tankNum - 1].gradient = gradientNumerator/
			gradientDenominator;

	int lowLevelNeg = -1*lowLevel;

	sensor_data->tankInters[tankNum - 1].intercept =
			sensor_data->tankInters[tankNum - 1].gradient*lowLevelNeg + 0;

	// using the percent variable, calculate a cm threshold
	sensor_data->tankThresholds[tankNum - 1] = ((-1*sensor_data->tankInters[tankNum - 1].intercept) + percent)/
							sensor_data->tankInters[tankNum - 1].gradient;

}

/*!
 * \brief Function that will parse the radio packet and return
 * an array of the packet contents
 *
 * src|dest|water|temp
 *
 */
char** parse_packet(char* packet, uint8_t *length) {

	for (int i = 0; i < strlen(packet); i++) {
		// Store the number of packet contents
		if (packet[i] == '|') {
			(*length)++;
		}
	}
	// Store each packet
	char **packetContents = malloc((*length) * sizeof(char*));

	int j = 0, subIndex = 0;
	for (int l = 0; l < (*length); l++) {
		subIndex = 0;
		char subPacket[15];
		while (packet[j] != '|') {
			// go through each packet in the message
			subPacket[subIndex] = packet[j];
			subIndex++;
			j++;
		}
		subPacket[subIndex] = '\0';

		// store the packet in a string array
		packetContents[l] = malloc((subIndex+1) * sizeof(char));
		memcpy(packetContents[l], &subPacket[0], subIndex+1);
		j++;
	}

	return packetContents;

}

/*!
 * \brief Function that will receive the configuration properties
 * from the base station.
*/
currentStatus_t get_node_data( GatewayData_t * gateway_data , SensorData_t * sensor_data) {

	uint8_t i = 0;
	char packet[30];
	char currentNode[6];
	uint8_t loraPacket[20];

	int32_t time = 0;

	static int init = TRUE;

	uint8_t noError = TRUE;
	int32_t wakeTime = HW_RTC_GetTimerSeconds();

	uint8_t firstLoop = 1;
	int32_t RecieveTime = 0;

	PRINTF("Requesting Data... Nodes: %d\n\r", gateway_data->totalNodes);

	for (i = 0; i < gateway_data->totalNodes; i++) {

		if (i == sensor_data->skipTank-1) {
			RecieveTime += 5;
			continue;
		}

		// To init the node!

		if (sensor_data->didNotReport[i] == 1) {
			if (sensor_data->didNotReportCnt[i] == 3) {
				sensor_data->didNotReportCnt[i] = 0;
				sensor_data->didNotReportTotal[i] += 1;
				PRINTF("Skipping Node %d, not responding - %d!\n\r", (int)i+1 ,
						sensor_data->didNotReportTotal[i]);
				RecieveTime += 5;
				if (!gateway_data->nodeNoResponseAck) {
					char *message = malloc(sizeof(char) * 60);
					sprintf(message, "Tank %d hasn't responded for the last %d minutes.",
							((int)i+1),
							sensor_data->didNotReportTotal[i]*15);
					send3Gsms(message, NULL, gateway_data);
					gateway_data->lastMessageSent = NODE_NO_RESPOND;
					free(message);
				}
				if (sensor_data->didNotReportTotal[i] == 7) {
					gateway_data->nodeNoResponseAck = 0;
				}
				continue;

			} else {
				// Delay to make sure there still is a space between nodes.
				DelayMs(5000);
			}
		}

		int node = i;

		sprintf(currentNode, "0x%02x", (node+1));
		//sprintf(packet, "0x02|0x00|I|");
		sprintf(packet, "%s|0x00|I|", currentNode);
		memcpy(loraPacket, packet, strlen(packet)+1);

		PRINTF("Sending to Node ID: %s \n\r", currentNode);
		Radio.Send( loraPacket, strlen(packet) + 1 );

		// Get the node data

		State = GATEWAY_DATA_WAITING;
		Radio.Rx( RX_TIMEOUT_VALUE );

		while (State == GATEWAY_DATA_WAITING);

		RecieveTime = HW_RTC_GetTimerSeconds() - wakeTime;

		PRINTF("Rx: %d secs\n\r", RecieveTime);

		switch (State) {
			case GATEWAY_DATA_RECV:
				break;
			case GATEWAY_DATA_TIMEOUT:
				// Timeout
				gateway_data->sleepTime = HW_RTC_GetTimerSeconds() - (RX_TIMEOUT_VALUE/CLOCK_SECOND) - 5;
				// node (gateway_data->totalNodes) has not reported
				PRINTF("Node %d has failed to report (Timeout).\n\r", (i+1));
				sensor_data->didNotReport[i] = 1;
				sensor_data->didNotReportCnt[i] += 1;
				return GATEWAY_DATA_ERROR;
			case GATEWAY_DATA_ERROR:
				// This means that the gateway could not recv data successfully
				noError = FALSE;
				//sensor_data->didNotReport[i] = 1;
				node++;
				break;
			default:
				break;
		}

		if (BufferSize > 0 && noError) {
			char dest[5];
			char src[5];
			char payload[20];
			memcpy( src, &Buffer[5], 4 );
			memcpy( dest, &Buffer[0], 4 );
			src[4] = '\0';
			dest[4] = '\0';
			// Check if the packet is for gateway
			if ((strcmp(dest, "0x00") == 0)) {
				node = strtol(src, NULL, 0);
				// Check to see if packet is from current node
				if (node != (i+1)) {
					PRINTF("BadPacket: Node %d\n\r", node);
					sprintf(currentNode, "0x%02x", node);
					for (int n = (i+1); n <= gateway_data->totalNodes; n++) {
						// Check to see if nodes havent responded.
						if (node == n) {
						// That means that (n-(i+1)) node/s have not reported
							for (int j = (n-1); j >= (i+1); j--) {
								wakeTime -= 5;
								PRINTF("Node %d has failed to report "
										"(overtaken by another node).\n\r", j);
								sensor_data->didNotReport[j-1] = 1;
								sensor_data->didNotReportCnt[j-1] += 1;

							}
						}
					}
					i = node-1;

				}
				memcpy(payload, &Buffer[10], BufferSize - 9);
				payload[BufferSize - 9] = '\0';
				PRINTF("***Recv data from Node ID: %d!\n\r", node);
				sensor_data->didNotReport[node-1] = 0;
				sensor_data->didNotReportCnt[node-1] = 0;
				uint8_t numPackets = 0;
				char **packetContents = parse_packet(payload, &numPackets);
				if (numPackets != 4) {
					PRINTF("Corrupt packet!\n\r");
					return GATEWAY_TRIG_FAILURE;
				}
				// Now we can index the packets via numPackets
				// Format should be:	packet[0] = water
				//						packet[1] = temperature
				//						packet[2] = voltage
				//						packet[3] = current

				// water
				char readings[10];
				if (packetContents[0][0] == 'W') {
					memcpy(readings, &packetContents[0][1], strlen(packetContents[0]));
					sensor_data->tankReadings[node-1] = 2*atoi(readings);
					PRINTF("Water: %d cm, %d %%\n\r", sensor_data->tankReadings[node-1],
							(int) (sensor_data->tankInters[node-1].gradient*
									sensor_data->tankReadings[node-1] +
							sensor_data->tankInters[node-1].intercept));
				}
				// temperature
				if (packetContents[1][0] == 'T') {
					memcpy(readings, &packetContents[1][1], strlen(packetContents[1]));
					sensor_data->temperatureReadings[node-1] = atof(readings);
					PRINTF("Temp: %f *C\n\r", sensor_data->temperatureReadings[node-1]);
				}
				if (numPackets > 2) {
					// Voltage
					if (packetContents[2][0] == 'V') {
						memcpy(readings, &packetContents[2][1], strlen(packetContents[2]));
						sensor_data->battVoltageReadings[node-1] = atof(readings);
						PRINTF("Voltage: %d mV\n\r", sensor_data->battVoltageReadings[node-1]);
					}
					// Current
					char *current;
					if (packetContents[3][0] == 'C') {
						memcpy(readings, &packetContents[3][1], strlen(packetContents[3]));
						sensor_data->averageCurrentReadings[node-1] = strtol(readings, &current, 10);


						PRINTF("Current: %d uA\n\r", sensor_data->averageCurrentReadings[node-1]);
					}
				}
				// RSSI
				PRINTF("RSSI: %d dBm\n\r", rssiValue);

				for (int i = 0; i < numPackets; i++) {
					free(packetContents[i]);
				}
				free(packetContents);


			} else {
				return GATEWAY_TRIG_FAILURE;
			}
		}

		if (gateway_data->updateReport) {
			gateway_data->reportInterval = gateway_data->updateReport;
			gateway_data->updateReport = 0;
		}

		time = 0;

		if (init || (gateway_data->ignoreRxTime == 1)) {
			time = 0;
		} else {
			time = (((i+1)*5) - RecieveTime);


			if (time < -2) {
				time = -2;
			} else if (time < 0) {
				time = -1;
			} else if (time > 2) {
				time = 2;
			} else if (time > 0) {
				time = 1;
			}

		}
		noError = TRUE;

		sprintf(packet, "%s|0x00|%03d|%d|", currentNode,
				(int) gateway_data->reportInterval,
				(int) (time));
		memcpy(loraPacket, packet, strlen(packet)+1);

		PRINTF("***Sending time to %d: %s\n\r", node, packet);
		State = GATEWAY_DATA_SEND;
		Radio.Send( loraPacket, strlen(packet)+1 );

		DelayMs(300);

		if (init && !REPROGRAM) {
			DelayMs(5000);
		}

		if (firstLoop) {
			gateway_data->sleepTime = HW_RTC_GetTimerSeconds();
			firstLoop = 0;
		} else if (sensor_data->didNotReport[0] == 1) {
			gateway_data->sleepTime = HW_RTC_GetTimerSeconds();
		}

	}
	init = FALSE;
	gateway_data->sleepInterval = gateway_data->reportInterval - CLOCK_SECOND*5;
	gateway_data->updateReport = 0;
	return GATEWAY_TRIG_SUCCESS;

}

/**
 * This function will get the new sensor data and alert if necessary
 *
 */
uint32_t check_readings( GatewayData_t * gateway_data, SensorData_t * sensor_data, uint16_t totalNodes ) {

	uint32_t errorFlags = 0;

	for (int i = 0, bit = 1; i < totalNodes; i++, bit*=2) {

		PRINTF("Reading: %d, thres: %d\n\r", sensor_data->tankReadings[i],
					sensor_data->tankThresholds[i]);

		if (sensor_data->tankReadings[i] >= sensor_data->tankThresholds[i]) {
			// This tank has an error, mask that bit number
			errorFlags |= bit;
			sensor_data->tankHasError[i] = 1;
		}

		// Make sure that the error tank has risen 20% above threshold
		if (sensor_data->tankHasError[i] &&
				(sensor_data->tankReadings[i] < sensor_data->tankThresholds[i])) {

			gateway_data->userAck = 1;

			// check that it is greater than 20% (103/128 = 0.80)%
			if (sensor_data->tankReadings[i] <
				(sensor_data->tankThresholds[i]*103/128)) {

				gateway_data->userAck = 0;
				sensor_data->tankHasError[i] = 0;

			} else {
				gateway_data->userAck &= 1;

			}
		}
	}

	if (!sensor_data->pressure) {
		errorFlags |= 0xF0;
	}

	PRINTF("Error Stat: %d\n\r", errorFlags);
	return errorFlags;

}

uint32_t check_battery( GatewayData_t * gateway_data,
		SensorData_t * sensor_data, uint16_t totalNodes ) {

	uint32_t errorFlags = 0;

	for (int i = 0, bit = 1; i < totalNodes; i++, bit*=2) {

		if (sensor_data->battVoltageReadings[i] <= 3500) {
			// This node has an low battery, mask that bit number
			errorFlags |= bit;
		}
	}

	return errorFlags;

}


void ble_init(void) {

	uint8_t SERVER_BDADDR[] = {0xaa, 0x00, 0x00, 0xE1, 0x80, 0x02};
	uint8_t bdaddr[6];
	uint16_t service_handle, dev_name_char_handle, appearance_char_handle;

	uint8_t  hwVersion;
    uint16_t fwVersion;

    /* Initialize the BlueNRG SPI driver */
    BNRG_SPI_Init();

    HCI_Init();

    /* Reset BlueNRG hardware */
    BlueNRG_RST();

    /* get the BlueNRG HW and FW versions */
    getBlueNRGVersion(&hwVersion, &fwVersion);

    /*
	 * Reset BlueNRG again otherwise we won't
	 * be able to change its MAC address.
	 * aci_hal_write_config_data() must be the first
	 * command after reset otherwise it will fail.
	 */
	BlueNRG_RST();

	int ret;

	PRINTF("HWver %d, FWver %d\n\r", hwVersion, fwVersion);

	if (hwVersion > 0x30) { /* X-NUCLEO-IDB05A1 expansion board is used */
		//bnrg_expansion_board = 1;
	  }
	Osal_MemCpy(bdaddr, SERVER_BDADDR, sizeof(SERVER_BDADDR));

	ret = aci_hal_write_config_data(0, 6, bdaddr);

	if(ret){
	  PRINTF("Setting BD_ADDR failed 0x%02x.\n\r", ret);
	}

	ret = aci_gatt_init();
	if(ret){
	  PRINTF("GATT_Init failed.\n\r");
	}

	ret = aci_gap_init_IDB05A1(1, 0, 0x07,
			&service_handle, &dev_name_char_handle, &appearance_char_handle);

  if(ret != 0){
	PRINTF("GAP_Init failed.\n\r");
  }

  ret = aci_gap_set_auth_requirement(MITM_PROTECTION_REQUIRED,
									 OOB_AUTH_DATA_ABSENT,
									 NULL,
									 7,
									 16,
									 USE_FIXED_PIN_FOR_PAIRING,
									 123456,
									 BONDING);
  if (ret == BLE_STATUS_SUCCESS) {
	PRINTF("BLE Stack Initialized.\n\r");
  }
  ret = Add_Sample_Service();

  if(ret == BLE_STATUS_SUCCESS)
	  PRINTF("Service added successfully.\n\r");
  else
	  PRINTF("Error while adding service.\n\r");

  /* Set output power level */
  ret = aci_hal_set_tx_power_level(1,4);

  TimerInit(&BLETimer, no_user);
  TimerSetValue( &BLETimer, CLOCK_SECOND*15);

}


/**
 * Given a string to send, this function will transmit the BLE message to the user.
 */
void sendBLE(const char *dataToSend ) {

	uint8_t *dataBuffer = malloc(sizeof(uint8_t) * 21);
	uint8_t j = 0;
	for (int i = 0; i < strlen(dataToSend); i++, j++) {
		if ((i != 0) && (i % 20 == 0)) {
			sendData(dataBuffer, 20);
			DelayMs(10);
			dataBuffer = realloc(dataBuffer, sizeof(uint8_t) * 21);
			j = 0;
		}

		dataBuffer[j] = (uint8_t)dataToSend[i];
	}

	sendData(dataBuffer, j);
	DelayMs(10);

	free(dataBuffer);

}

void start_ble_connection( uint32_t timeDiff, uint32_t sleepTime, GatewayData_t * gateway_data, SensorData_t * sensor_data ) {

	Make_Connection();

	uint32_t timeAsleep = timeDiff + 2;

	uint32_t sleepForMs = sleepTime;



	TimerStart(&BLETimer);

	uint32_t currentTimer = 0, currentTimeCount = HW_RTC_GetTimerSeconds();

	int errorBeforeConnection = FALSE;

	while (!connected) {
		HCI_Process();

		if (noUserFlag) {
			TimerStop(&BLETimer);
			noUserFlag = FALSE;
			return;
		}

		currentTimer = HW_RTC_GetTimerSeconds() - currentTimeCount;

		// Check and see if the sampling timer is about to expire
		if ((timeAsleep*1000 + currentTimer*CLOCK_SECOND)
				>= HW_RTC_ms2Tick(sleepTime)) {
			gateway_data->bleForcedSample = TRUE;
			timeAsleep = 0;
			int ret = get_node_data(gateway_data, sensor_data);

			currentTimeCount = HW_RTC_GetTimerSeconds();
			currentTimer = 0;
			HW_RTC_StopAlarm();
			DelayMs(100);
			HW_RTC_SetAlarm(gateway_data->sleepInterval);

			if (ret == GATEWAY_TRIG_FAILURE) {
				PRINTF("Failed to get node data!\n\r");
			}

			uint32_t errorFlags = check_readings(gateway_data, sensor_data, gateway_data->totalNodes);
			if (errorFlags != 0) {
				// if one of the readings is low
				errorBeforeConnection = TRUE;
			}

			sleepForMs = gateway_data->sleepInterval -
						(HW_RTC_GetTimerSeconds() - gateway_data->sleepTime)*CLOCK_SECOND;
		}
	}
	TimerStop(&BLETimer);
	noUserFlag = FALSE;

	// Send command list
	DelayMs(1300);
	sendBLE("Welcome!\n");
	send_command_list();

	currentTimer = 0, currentTimeCount = HW_RTC_GetTimerSeconds();

	TimerSetValue( &BLETimer, 60*1000*2);

	//TimerStart(&BLETimer);

	PRINTF("Sleepin: %d, time: %d\n\r", sleepForMs, timeAsleep);

	while (connected) {

		// Wait until user disconnects from gateway
		HCI_Process();
		if (messageRecv) {
			parseBLEMesg(BLEUserMesg, gateway_data, sensor_data);
			messageRecv = FALSE;
		}

		currentTimer = HW_RTC_GetTimerSeconds() - currentTimeCount;

		//PRINTF("T: %d, C: %d, Tot: %d\n\r", timeAsleep, currentTimer,
			//	(timeAsleep)*CLOCK_SECOND + currentTimer*CLOCK_SECOND);

		// Check and see if the sampling timer is about to expire
		if (((timeAsleep)*CLOCK_SECOND + currentTimer*CLOCK_SECOND)
				>= HW_RTC_ms2Tick(sleepForMs)) {

			sendBLE("\n****Sampling - Please wait!****\n");
			gateway_data->bleForcedSample = TRUE;
			timeAsleep = 0;
			int ret = get_node_data(gateway_data, sensor_data);

			currentTimeCount = HW_RTC_GetTimerSeconds();
			currentTimer = 0;

			if (ret == GATEWAY_TRIG_FAILURE) {
				PRINTF("Failed to get node data!\n\r");
			}
			char tanks[19];
			for (int i = 0; i < gateway_data->totalNodes; i++) {
				sprintf(tanks, "Tank %d: %d %%, %.2f\n", (i+1),
						(int) (sensor_data->tankInters[i].gradient*sensor_data->tankReadings[i] +
													sensor_data->tankInters[i].intercept),
													sensor_data->temperatureReadings[i]);
				sendBLE(tanks);
			}

			sendBLE("\n****Finished - Please continue!****\n");

			uint32_t errorFlags = check_readings(gateway_data, sensor_data, gateway_data->totalNodes);
			if (errorFlags != 0) {
				// if one of the readings is low
				sendBLE("TANKS LOW!\n");
				sendBLE("Since YOU see error, ");
				sendBLE("I will stop texts\n\n");
				gateway_data->userAck = 1;
			}

			sleepForMs = gateway_data->sleepInterval -
							  (HW_RTC_GetTimerSeconds() - gateway_data->sleepTime)*CLOCK_SECOND;
			  if ((sleepForMs <= 0) || (sleepForMs > (gateway_data->sleepInterval + 1*CLOCK_MINUTE))) {
				  sleepForMs = 1*CLOCK_MINUTE;
				  RX_TIMEOUT_VALUE = gateway_data->sleepInterval;
				  gateway_data->ignoreRxTime = 1;
			  } else {
				  RX_TIMEOUT_VALUE = 40000;
				  gateway_data->ignoreRxTime = 0;
			  }
			HW_RTC_StopAlarm();
			DelayMs(100);
			HW_RTC_SetAlarm(HW_RTC_ms2Tick(sleepForMs));

		}

		if (errorBeforeConnection) {
			sendBLE("TANKS LOW!\n");
			char tanks[19];
			for (int i = 0; i < gateway_data->totalNodes; i++) {
				sprintf(tanks, "Tank %d: %d %%\n", (i+1),
						(int) (sensor_data->tankInters[i].gradient*sensor_data->tankReadings[i] +
													sensor_data->tankInters[i].intercept));
				sendBLE(tanks);
			}
			sendBLE("Since YOU see error, ");
			sendBLE("I will stop texts\n\n");
			gateway_data->userAck = 1;
		}

		if (noUserFlag) {
			TimerStop(&BLETimer);
			noUserFlag = FALSE;
			sendBLE("DISCONNECTING!");
			ble_init();
			return;
		}
	}
	TimerStop(&BLETimer);
}

/*
 * Parses the recv BLE message and sends a response back for the user
 */
void parseBLEMesg( messageType_t bleCommand, GatewayData_t * gateway_data, SensorData_t * sensor_data) {

	switch (bleCommand) {

		case HELP:
			send_command_list();
			break;
		case TANKS:
			sendBLE("\nTANK READINGS\n");
			char tanks[19];
			for (int i = 0; i < gateway_data->totalNodes; i++) {
				sprintf(tanks, "Tank %d: %d cm, %d %%\n", (i+1),
						sensor_data->tankReadings[i],
						(int) (sensor_data->tankInters[i].gradient*sensor_data->tankReadings[i] +
								sensor_data->tankInters[i].intercept));
				sendBLE(tanks);
			}
			sendBLE("\n");
			break;
		case TEMPS:
			sendBLE("\nTEMP READINGS\n");
			char temps[19];
			for (int i = 0; i < gateway_data->totalNodes; i++) {
				sprintf(temps, "Temp %d: %2.2f *C\n", (i+1), sensor_data->temperatureReadings[i]);
				sendBLE(temps);
			}
			sendBLE("\n");
			break;
		case LEVEL:

			// update tank number
			PRINTF("Update Tank %d: H:%d cm, L:%d cm\n\r", tankNumber,
					tankHighReading, tankLowReading);
			updateTankLevels(sensor_data, tankNumber, tankHighReading, tankLowReading, 20);
			sendBLE("Updated Level!\n");
			break;
		case SET_NUM:
			PRINTF("phNum: %s\n\r", phNum);
			if (add_eeprom_num(priority-1, phNum) == FALSE) {
				sendBLE("Number Couldn't be added\n\n");
			}
			sendBLE("Added Number!\n");
			break;
		case NODES:
			gateway_data->totalNodes = tankNumber;
			// reallocate space for new nodes
			sensor_data->tankReadings = realloc(sensor_data->tankReadings,
					sizeof(uint16_t)*gateway_data->totalNodes);
			sensor_data->temperatureReadings = realloc(sensor_data->temperatureReadings,
					sizeof(double)*gateway_data->totalNodes);
			sensor_data->battVoltageReadings = realloc(sensor_data->battVoltageReadings,
					sizeof(uint16_t)*gateway_data->totalNodes);
			sensor_data->averageCurrentReadings = realloc(sensor_data->averageCurrentReadings,
					sizeof(uint32_t)*gateway_data->totalNodes);
			sensor_data->tankInters = realloc(sensor_data->tankInters,
					sizeof(TankInterpolation_t)*gateway_data->totalNodes);
			sensor_data->tankThresholds = realloc(sensor_data->tankThresholds,
					sizeof(uint16_t)*gateway_data->totalNodes);
			sensor_data->tankHasError = realloc(sensor_data->tankHasError,
					sizeof(uint16_t)*gateway_data->totalNodes);
			sensor_data->didNotReport = realloc(sensor_data->didNotReport,
					sizeof(uint16_t)*gateway_data->totalNodes);
			sensor_data->didNotReportCnt = realloc(sensor_data->didNotReportCnt,
					sizeof(uint16_t)*gateway_data->totalNodes);
			sensor_data->didNotReportTotal = realloc(sensor_data->didNotReportTotal,
					sizeof(uint16_t)*gateway_data->totalNodes);

			for (int i = 0; i < gateway_data->totalNodes; i++) {
				sensor_data->tankReadings[i] = 0;
				sensor_data->temperatureReadings[i] = 0;
				sensor_data->averageCurrentReadings[i] = 0;
				sensor_data->battVoltageReadings[i] = 0;
				sensor_data->tankInters[i].gradient = 1;
				sensor_data->tankInters[i].intercept = 1;
				sensor_data->tankThresholds[i] = 600;
				sensor_data->tankHasError[i] = 0;
				sensor_data->didNotReport[i] = 0;
				sensor_data->didNotReportCnt[i] = 0;
				sensor_data->didNotReportTotal[i] = 0;
			}
			sendBLE("Updated Node Count!\n");
			break;
		case NUMS:
			sendBLE("Numbers\n");
			char message[40];
			char *number = malloc(sizeof(char) * 11);
			int count = get_eeprom_max_nums();
			for (int i = 0; i < count; i++) {
				get_eeprom_num(i, &number);
				number[10] = '\0';
				sprintf(message, "%d: %s\n", (i+1), number);
				sendBLE(message);
				number = realloc(number, sizeof(char) * 12);

			}
			sendBLE("\n");
			break;

		case REMOVE:
			init_eeprom();
			break;
		case REPORT:
			gateway_data->updateReport = tankNumber*1000;
			PRINTF("Report update: %d\n\r", gateway_data->updateReport);
			char bleMessage[17];
			sprintf(bleMessage, "Report: %d\n", (int)gateway_data->updateReport);
			sendBLE(bleMessage);
			break;
		case VOLTAGES:
			sendBLE("\nVOLTAGE READINGS\n");
			char voltages[19];
			for (int i = 0; i < gateway_data->totalNodes; i++) {
				sprintf(voltages, "Voltage %d: %d mV\n", (i+1), sensor_data->battVoltageReadings[i]);
				sendBLE(voltages);
			}
			sendBLE("\n");
			break;
		case CURRENTS:
			sendBLE("\nCURRENT READINGS\n");
			char currents[19];
			for (int i = 0; i < gateway_data->totalNodes; i++) {
				sprintf(currents, "Current %d: %d uA\n", (i+1), (int) sensor_data->averageCurrentReadings[i]);
				sendBLE(currents);
			}
			sendBLE("\n");
			break;

	}
}

/*
 * Will send the command list.
 * Called on startup or when 'help' is requested.
 */
void send_command_list( void ) {

	sendBLE("Command List:\n");
	sendBLE("1.'Help' -> get command list\n");
	sendBLE("2.'Tanks' -> get tank levels\n");
	sendBLE("3.'Temps' -> get temp levels\n");
	sendBLE("4.'Level [num] [high] [low]' -> set high, low level of num tank in cm\n");
	sendBLE("5.'Set [priority] [num]' -> set Ph. Num\n");
	sendBLE("6.'Nums' -> get all Ph. Nums\n");
	sendBLE("7.'Remove' -> remove all Ph. Num\n");
	sendBLE("8.'Nodes [number]' -> set num of sensors\n");
	sendBLE("9.'Report [number]' -> set report interval\n");
	sendBLE("\n");

}

/*!
 * \brief Init the radio module.
*/
void LoraRadio_Init( void ) {

	// Radio initialization
	RadioEvents.TxDone = OnTxDone;
	RadioEvents.RxDone = OnRxDone;
	RadioEvents.TxTimeout = OnTxTimeout;
	RadioEvents.RxTimeout = OnRxTimeout;
	RadioEvents.RxError = OnRxError;

	Radio.Init( &RadioEvents );

	Radio.SetChannel( RF_FREQUENCY );

	Radio.SetTxConfig( MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
	LORA_SPREADING_FACTOR, LORA_CODINGRATE,
	  LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
	  true, 0, 0, LORA_IQ_INVERSION_ON, 3000000 );

	Radio.SetRxConfig( MODEM_LORA, LORA_BANDWIDTH, LORA_SPREADING_FACTOR,
	  LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
	  LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_ON,
	  0, true, 0, 0, LORA_IQ_INVERSION_ON, true );

}

/*!
 * \brief In this function, the state of the GPIOx_MODER registers are
*               save into global variables GPIOA_MODER, GPIOB_MODER, and
*               GPIOC_MODER. The MODER registers are all then configured to
*               analog mode
*/
void Idd_SaveContext( void )
{
	char was_waiting = 0;

	// disable interrupts if they weren't already disabled
	if ( __get_PRIMASK() )
	{
	was_waiting = 1;
	}
	else
	{
	__disable_irq();
	}

	// Enable GPIO clocks
	RCC->IOPENR |= RCC_IOPENR_GPIOAEN | RCC_IOPENR_GPIOBEN | RCC_IOPENR_GPIOCEN;

	GPIOA_MODER = GPIOA->MODER;  // dummy read

	// Save the current mode of the I/O pins
	GPIOA_MODER = GPIOA->MODER;
	GPIOB_MODER = GPIOB->MODER;
	GPIOC_MODER = GPIOC->MODER;

	   // Configure GPIO port pins in Analog Input mode
	GPIOA->MODER = 0xFFFFFFFF;
	GPIOB->MODER = 0xFFFFFFFF;
	GPIOC->MODER = 0xFFFFFFFF;

	// Leave the external interrupts alone!
	GPIOC->MODER &= ~( GPIO_MODER_MODE13 ); // Input mode
	GPIOA->MODER &= ~( GPIO_MODER_MODE0 );

	// Disable GPIO clocks
	RCC->IOPENR &= ~( RCC_IOPENR_GPIOAEN | RCC_IOPENR_GPIOBEN | RCC_IOPENR_GPIOCEN );

	// enable interrupts if they were enabled before this function was called
	if ( !was_waiting )
	{
	__enable_irq();
	}
}

/*!
 * \brief Function to be restore GPIO registers
 */
void Idd_RestoreContext(void)
{
	char was_waiting = 0;

	// disable interrupts if they weren't already disabled
	if ( __get_PRIMASK() )
	{
	was_waiting = 1;
	}
	else
	{
	__disable_irq();
	}

	// Enable GPIO clocks
	RCC->IOPENR |= RCC_IOPENR_GPIOAEN | RCC_IOPENR_GPIOBEN | RCC_IOPENR_GPIOCEN;

	GPIOA->MODER = GPIOA_MODER; // dummy write

	// Restore the previous mode of the I/O pins
	GPIOA->MODER = GPIOA_MODER;
	GPIOB->MODER = GPIOB_MODER;
	GPIOC->MODER = GPIOC_MODER;

	// enable interrupts if they were enabled before this function was called
	if ( !was_waiting )
	{
	__enable_irq();
	}
}

void Enter_Stop( void )
{
	PRINTF("Sleeping sensors and stopping MCU\n\n\r");

	// Enable Clocks
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;
	 RCC->IOPENR |= RCC_IOPENR_GPIOAEN;

   //Prepare to enter stop mode
	 PWR->CR |= PWR_CR_CWUF;      // clear the WUF flag after 2 clock cycles
	 PWR->CR &= ~( PWR_CR_PDDS ); // Enter stop mode when the CPU enters deepsleep
	 RCC->CFGR |= RCC_CFGR_STOPWUCK; // HSI16 oscillator is wake-up from stop clock
	 SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk; // low-power mode = stop mode

	 __WFI(); // enter low-power mode
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void stm32l0_SystemClock_Config(void)
{

	RCC_OscInitTypeDef RCC_OscInitStruct;
	  RCC_ClkInitTypeDef RCC_ClkInitStruct;
	  RCC_PeriphCLKInitTypeDef PeriphClkInit;

	    /**Configure the main internal regulator output voltage
	    */
	  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

	    /**Initializes the CPU, AHB and APB busses clocks
	    */
	  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
	  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	  RCC_OscInitStruct.HSICalibrationValue = 16;
	  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
	  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
	  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	  {

	  }

	    /**Initializes the CPU, AHB and APB busses clocks
	    */
	  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
	                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
	  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
	  {

	  }

	  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_I2C1;
	  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
	  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
	  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	  {

	  }

	    /**Configure the Systick interrupt time
	    */
	  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

	    /**Configure the Systick
	    */
	  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	  /* SysTick_IRQn interrupt configuration */
	  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/*!
 * \brief Function to be set MCU into Low Power run mode
 */
void Enter_LPRun( void )
{
    /* 1. Each digital IP clock must be enabled or disabled by using the
                   RCC_APBxENR and RCC_AHBENR registers */
    RCC->APB1ENR |= RCC_APB1ENR_PWREN;
    /* 2. The frequency of the system clock must be decreased to not exceed the
          frequency of f_MSI range1. */

    stm32l0_SystemClock_Config();
    /* 3. The regulator is forced in low-power mode by software
          (LPRUN and LPSDSR bits set ) */
    PWR->CR &= ~PWR_CR_LPRUN; // Be sure LPRUN is cleared!

    PWR->CR |= PWR_CR_LPSDSR; // must be set before LPRUN
    PWR->CR |= PWR_CR_LPRUN; // enter low power run mode

    PRINTF("Woken - Gateway. System Uptime: %d secs\n\r", (int)HW_RTC_GetTimerSeconds());
}

/*!
 * \brief Function to be executed on Radio Tx Done event
 */
void OnTxDone( void )
{
    //Radio.Sleep( );
    State = GATEWAY_DATA_TX;
}

/*!
 * \brief Function to be executed on Radio Rx Done event
 */
void OnRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
    //Radio.Sleep( );
    BufferSize = size;
    memcpy( Buffer, payload, BufferSize );
    RssiValue = rssi;
    SnrValue = snr;
    //State = RX;

    rssiValue = rssi;
  
    //PRINTF("RssiValue=%d dBm, SnrValue=%d\n\r", rssi, snr);
    State = GATEWAY_DATA_RECV;
}

/*!
 * \brief Function executed on Radio Tx Timeout event
 */
void OnTxTimeout( void )
{
    //Radio.Sleep( );
    //State = TX_TIMEOUT;
  
    PRINTF("OnTxTimeout\n\r");
}

/*!
 * \brief Function executed on Radio Rx Timeout event
 */
void OnRxTimeout( void )
{
    //Radio.Sleep( );
    State = GATEWAY_DATA_TIMEOUT;
}

/*!
 * \brief Function executed on Radio Rx Error event
 */
void OnRxError( void )
{
    // Radio.Sleep( );
    State = GATEWAY_DATA_ERROR;
    PRINTF("OnRxError\n\r");
}

/* IRQ handler for the BLE connection */
void ble_wakeup( void ) {

	//PRINTF("WAKEUP\n\r");
	ble_wakeup_flag = TRUE;
}

void txt_wakeup( void ) {

	txt_wakeup_flag = TRUE;

	Linecount = -5;

	PRINTF("TEXT\n\r");


}

void bad_text ( void ) {



}

void no_user ( void ) {

	noUserFlag = TRUE;
}


/**
  * Brief   This function handles USART1 interrupt request.
  * Param   None
  * Retval  None
  */
void USART1_IRQHandler(void)
{
   uint8_t chartoreceive = 0;

   if((USART1->ISR & USART_ISR_TC) == USART_ISR_TC) {

	   if(send == strlen(usartToSend)) {
		   send=0;
		   USART1->ICR = USART_ICR_TCCF;/* Clear transfer complete flag */
		   txFinished = TRUE;
	   } else {


	  /* clear transfer complete flag and fill TDR with a new char */
		   USART1->TDR = usartToSend[send++];

	}
  } else if((USART1->ISR & USART_ISR_RXNE) == USART_ISR_RXNE) {

	  chartoreceive = (uint8_t)(USART1->RDR);/* Receive data, clear flag */

	  if (chartoreceive == '>') {
		  sendingMessage = 1;
	  }

	  if (chartoreceive == '\r') {
		  Linecount++;
		  if (Linecount == 2) {
			  Linecount = 0;
			  if (strstr(usartReceived, "OK") != NULL) {
				  // line finished - okay
				  bufferCount = 0;
				  ERROR_CODE = OKAY;
				  if (getMesg) {
					  strcpy(textMesg, usartReceived);
				  }
				  recvFinished = TRUE;
			  } else if (strstr(usartReceived, "ERROR") != NULL) {
				  // line finished - error
				  bufferCount = 0;
				  ERROR_CODE = USART;
				  recvFinished = TRUE;
			  }

			  strcpy(usartReceived, " ");
			  bufferCount = 0;
		  }
	  } else {
		  usartReceived[bufferCount] = chartoreceive;

		  if (recvFinished != TRUE) {
			  recvFinished = FALSE;
		  }
		  bufferCount++;
	  }

  } else {

	  PRINTF("ERROR ON USART\n\r");
	  NVIC_DisableIRQ(USART1_IRQn); /* Disable USART1_IRQn */
  }
}






