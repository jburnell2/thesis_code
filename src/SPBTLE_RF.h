/*
 * SPBTLE_RF.h
 *
 *  Created on: 13May,2018
 *      Author: Jezz
 */

#ifndef PROJECTS_PINGPONG_SPBTLE_RF_H_
#define PROJECTS_PINGPONG_SPBTLE_RF_H_

#include "stm32l0xx_hal.h"

// SPI Instance
#define BNRG_SPI_INSTANCE		        SPI2
#define BNRG_SPI_CLK_ENABLE()		    __SPI2_CLK_ENABLE()

// SPI Configuration
#define BNRG_SPI_MODE			        SPI_MODE_MASTER
#define BNRG_SPI_DIRECTION		        SPI_DIRECTION_2LINES
#define BNRG_SPI_DATASIZE		        SPI_DATASIZE_8BIT
#define BNRG_SPI_CLKPOLARITY		    SPI_POLARITY_LOW
#define BNRG_SPI_CLKPHASE	        	SPI_PHASE_1EDGE
#define BNRG_SPI_NSS			        SPI_NSS_SOFT
#define BNRG_SPI_FIRSTBIT	        	SPI_FIRSTBIT_MSB
#define BNRG_SPI_TIMODE		        	SPI_TIMODE_DISABLED
#define BNRG_SPI_CRCPOLYNOMIAL	        7
#define BNRG_SPI_BAUDRATEPRESCALER      SPI_BAUDRATEPRESCALER_4
#define BNRG_SPI_CRCCALCULATION		    SPI_CRCCALCULATION_DISABLED

// SPI Reset Pin: PA.8
#define BNRG_SPI_RESET_PIN	        	GPIO_PIN_8
#define BNRG_SPI_RESET_MODE	        	GPIO_MODE_OUTPUT_PP
#define BNRG_SPI_RESET_PULL	        	GPIO_PULLUP
#define BNRG_SPI_RESET_SPEED		    GPIO_SPEED_LOW
#define BNRG_SPI_RESET_ALTERNATE	    0
#define BNRG_SPI_RESET_PORT		        GPIOA
#define BNRG_SPI_RESET_CLK_ENABLE()     __GPIOA_CLK_ENABLE()

// SCLK: PB.13
#define BNRG_SPI_SCLK_PIN		        GPIO_PIN_13
#define BNRG_SPI_SCLK_MODE		        GPIO_MODE_AF_PP
#define BNRG_SPI_SCLK_PULL		        GPIO_PULLDOWN
#define BNRG_SPI_SCLK_SPEED		        GPIO_SPEED_HIGH
#define BNRG_SPI_SCLK_ALTERNATE		    GPIO_AF0_SPI2
#define BNRG_SPI_SCLK_PORT		        GPIOB
#define BNRG_SPI_SCLK_CLK_ENABLE()	    __GPIOB_CLK_ENABLE()

// MISO (Master Input Slave Output): PB.14
#define BNRG_SPI_MISO_PIN		        GPIO_PIN_14
#define BNRG_SPI_MISO_MODE		        GPIO_MODE_AF_PP
#define BNRG_SPI_MISO_PULL		        GPIO_PULLUP
#define BNRG_SPI_MISO_SPEED		        GPIO_SPEED_HIGH
#define BNRG_SPI_MISO_ALTERNATE		    GPIO_AF0_SPI2
#define BNRG_SPI_MISO_PORT		        GPIOB
#define BNRG_SPI_MISO_CLK_ENABLE()	    __GPIOB_CLK_ENABLE()

// MOSI (Master Output Slave Input): PB.15
#define BNRG_SPI_MOSI_PIN			GPIO_PIN_15
#define BNRG_SPI_MOSI_MODE			GPIO_MODE_AF_PP
#define BNRG_SPI_MOSI_PULL			GPIO_NOPULL
#define BNRG_SPI_MOSI_SPEED			GPIO_SPEED_HIGH
#define BNRG_SPI_MOSI_ALTERNATE		GPIO_AF0_SPI2
#define BNRG_SPI_MOSI_PORT			GPIOB
#define BNRG_SPI_MOSI_CLK_ENABLE()	__GPIOB_CLK_ENABLE()

// NSS/CSN/CS: PB.12
#define BNRG_SPI_CS_PIN				GPIO_PIN_12
#define BNRG_SPI_CS_MODE			GPIO_MODE_OUTPUT_PP
#define BNRG_SPI_CS_PULL			GPIO_PULLUP
#define BNRG_SPI_CS_SPEED			GPIO_SPEED_HIGH
#define BNRG_SPI_CS_ALTERNATE		0
#define BNRG_SPI_CS_PORT			GPIOB
#define BNRG_SPI_CS_CLK_ENABLE()	__GPIOB_CLK_ENABLE()

// IRQ: PA.0
#define BNRG_SPI_IRQ_PIN			GPIO_PIN_0
#define BNRG_SPI_IRQ_MODE			GPIO_MODE_IT_RISING
#define BNRG_SPI_IRQ_PULL			GPIO_NOPULL
#define BNRG_SPI_IRQ_SPEED			GPIO_SPEED_HIGH
#define BNRG_SPI_IRQ_ALTERNATE		0
#define BNRG_SPI_IRQ_PORT			GPIOA
#define BNRG_SPI_IRQ_CLK_ENABLE()	__GPIOA_CLK_ENABLE()

// EXTI External Interrupt for SPI
// NOTE: if you change the IRQ pin remember to implement a corresponding handler
// function like EXTI0_1_IRQHandler() in the user project
#define BNRG_SPI_EXTI_IRQn           EXTI0_1_IRQn
#define BNRG_SPI_EXTI_IRQHandler     EXTI0_1_IRQHandler
#define BNRG_SPI_EXTI_PIN            BNRG_SPI_IRQ_PIN
#define BNRG_SPI_EXTI_PORT           BNRG_SPI_IRQ_PORT
#define RTC_WAKEUP_IRQHandler        RTC_IRQHandler


void BNRG_SPI_Init(void);
void BlueNRG_RST(void);
uint8_t BlueNRG_DataPresent(void);
void    BlueNRG_HW_Bootloader(void);
int32_t BlueNRG_SPI_Read_All(uint8_t *buffer,
                             uint8_t buff_size);
int32_t BlueNRG_SPI_Write(uint8_t* data1,
                          uint8_t* data2,
                          uint8_t Nb_bytes1,
                          uint8_t Nb_bytes2);

HAL_StatusTypeDef HAL_SPI_TransmitReceive_Opt(const uint8_t *pTxData, uint8_t *pRxData, uint8_t Size);
HAL_StatusTypeDef HAL_SPI_Transmit_Opt(const uint8_t *pTxData, uint8_t Size);
HAL_StatusTypeDef HAL_SPI_Receive_Opt(uint8_t *pRxData, uint8_t Size);


#endif /* PROJECTS_PINGPONG_SPBTLE_RF_H_ */
