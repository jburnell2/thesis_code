/*
 * eeprom.c
 *
 *  Created on: 26Aug.,2018
 *      Author: Jezz
 */


/**
 *
 * Functions that deal with eeprom
 *
 * Index starts at 0 (index variable 0,1,2...)
 *
 */

#include "hal_types.h"
#include <string.h>
#include "hw.h"
#include "radio.h"
#include "timeServer.h"
#include "delay.h"
#include "low_power_manager.h"
#include "vcom.h"
#include "stm32l0xx_hal_rtc.h"


#define DATA_E2_ADDR   ((uint32_t)0x08080000)  /* Start EEPROM address */

#define MAX_PH_NUMS 	4

void UnlockPELOCK(void);
void LockNVM(void);
void EepromErase(uint32_t addr);


int init_eeprom( void ) {

	UnlockPELOCK();

	FLASH->PECR |= FLASH_PECR_ERRIE | FLASH_PECR_EOPIE; /* enable flash interrupts */

	for (int i = 0; i < MAX_PH_NUMS; i++) {

		*(uint8_t *)(DATA_E2_ADDR+((i*10))) = (uint8_t)'N'; /* (1) */
		DelayMs(100);
		if  (*(uint8_t *)(DATA_E2_ADDR+((i*10))) != (uint8_t)'N')
		{
		  return FALSE;
		}
	}

	LockNVM();

	return TRUE;

}

int get_eeprom_num_count( void ) {

	UnlockPELOCK();

	char phSpace;
	int numCount = 0;
	for (int i = 0; i < MAX_PH_NUMS; i++) {

		phSpace = (char)*(uint8_t *)(DATA_E2_ADDR+((i*10)));

		DelayMs(100);

		if (phSpace != (char)*(uint8_t *)(DATA_E2_ADDR+((i*10)))) {
			PRINTF("Couldn't get data\n\r");
		}

		if (phSpace == 'N') {
			numCount++;
		}

	}

	LockNVM();

	return numCount;
}


int add_eeprom_num( int index, char* string ) {


	UnlockPELOCK();


	/* Perform data programming */
	/* (1) Write in data EEPROM */

	for (int i = 0; i < 10; i++) {

		*(uint8_t *)(DATA_E2_ADDR+((index*10)+i)) = (uint8_t)string[i]; /* (1) */
		DelayMs(100);
		if  (*(uint8_t *)(DATA_E2_ADDR+((index*10)+i)) != (uint8_t)string[i])
		{
		  return FALSE;
		}
	}

	LockNVM();

	return TRUE;
}


void get_eeprom_num( int index, char** string ) {

	UnlockPELOCK();

	for (int i = 0; i < 10; i++) {

		(*string)[i] = (char)*(uint8_t *)(DATA_E2_ADDR+((index*10)+i));

		DelayMs(100);

		if ((*string)[i] != (char)*(uint8_t *)(DATA_E2_ADDR+((index*10)+i))) {
			PRINTF("Couldn't get data\n\r");
		}
	}

	LockNVM();


}


void remove_eeprom_num( int index ) {


}

/**
  * Brief   This function unlocks the data EEPROM and the FLASH_PECR.
  *         The data EEPROM will be ready to be erased or programmed
  *         but the program memory will be still locked till PRGLOCK is set.
  *         It first checks no flash operation is on going,
  *         then unlocks PELOCK if it is locked.
  * Param   None
  * Retval  None
  */
void UnlockPELOCK(void)
{
  /* (1) Wait till no operation is on going */
  /* (2) Check if the PELOCK is unlocked */
  /* (3) Perform unlock sequence */
  while ((FLASH->SR & FLASH_SR_BSY) != 0) /* (1) */
  {
    /* For robust implementation, add here time-out management */
  }
  if ((FLASH->PECR & FLASH_PECR_PELOCK) != 0) /* (2) */
  {
    FLASH->PEKEYR = FLASH_PEKEY1; /* (3) */
    FLASH->PEKEYR = FLASH_PEKEY2;
  }
}

/**
  * Brief   This function locks the NVM.
  *         It first checks no flash operation is on going,
  *         then locks the flash.
  * Param   None
  * Retval  None
  */
void LockNVM(void)
{
  /* (1) Wait till no operation is on going */
  /* (2) Locks the NVM by setting PELOCK in PECR */
  while ((FLASH->SR & FLASH_SR_BSY) != 0) /* (1) */
  {
    /* For robust implementation, add here time-out management */
  }
  FLASH->PECR |= FLASH_PECR_PELOCK; /* (2) */
}

/**
  * Brief   This function erases a word of data EEPROM.
  *         The ERASE bit and DATA bit are set in PECR at the beginning
  *         and reset at the endof the function. In case of successive erase,
  *         these two operations could be performed outside the function.
  *         The flash interrupts must have been enabled prior to call
  *         this function.
  * Param   addr is the 32-bt word address to erase
  * Retval  None
  */
void EepromErase(uint32_t addr)
{
  /* (1) Set the ERASE and DATA bits in the FLASH_PECR register
         to enable page erasing */
  /* (2) Write a 32-bit word value at the desired address
         to start the erase sequence */
  /* (3) Enter in wait for interrupt. The EOP check is done in the Flash ISR */
  /* (6) Reset the ERASE and DATA bits in the FLASH_PECR register
         to disable the page erase */
  FLASH->PECR |= FLASH_PECR_ERASE | FLASH_PECR_DATA; /* (1) */
  *(__IO uint32_t *)addr = (uint32_t)0; /* (2) */
  __WFI(); /* (3) */
  FLASH->PECR &= ~(FLASH_PECR_ERASE | FLASH_PECR_DATA); /* (4) */
}

/**
  * Brief   This function handles FLASH interrupt request.
  *         It handles any kind of error even if not used in this example.
  * Param   None
  * Retval  None
  */
void FLASH_IRQHandler(void)
{
  if ((FLASH->SR & FLASH_SR_EOP) != 0)  /* (3) */
  {
    FLASH->SR = FLASH_SR_EOP; /* (4) */
  }
}

