/*
 * eeprom.h
 *
 *  Created on: 26Aug.,2018
 *      Author: Jezz
 */

#ifndef PROJECTS_PINGPONG_EEPROM_H_
#define PROJECTS_PINGPONG_EEPROM_H_

#include "stm32l0xx_hal.h"


int get_eeprom_num_count( void );
int add_eeprom_num( int index, char* string );
void get_eeprom_num( int index, char** string );
void remove_eeprom_num( int index );
int init_eeprom( void );


#endif /* PROJECTS_PINGPONG_EEPROM_H_ */
