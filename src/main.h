/*
 * main.h
 *
 *  Created on: 4Sep.,2018
 *      Author: Jezz
 */

#ifndef PROJECTS_PINGPONG_MAIN_H_
#define PROJECTS_PINGPONG_MAIN_H_

#include "data_structs.h"
#include "hal_types.h"
#include <string.h>
#include "hw.h"
#include "radio.h"
#include "timeServer.h"
#include "delay.h"
#include "low_power_manager.h"
#include "vcom.h"
#include "stm32l0xx_hal_rtc.h"

uint32_t check_readings( GatewayData_t * gateway_data, SensorData_t * sensor_data, uint16_t totalNodes);
void updateTankLevels(SensorData_t *sensor_data, int tankNum, int highLevel, int lowLevel);
currentStatus_t get_node_data( GatewayData_t * gateway_data , SensorData_t * sensor_data);
void set_alarm( uint32_t timeout );

#endif /* PROJECTS_PINGPONG_MAIN_H_ */
